/*
 * Beam OS Settings Page entry-point
 */

$(document).ready(function() {

	BEAM.log("Initializing BEAM for Settings Page.");
	
	BEAM.init(true);
	
	var accountList = BeamStorageManager.getCollection(BeamStorageCollections.CLOUD_PRIORITY_KEY);
	$.each(accountList, function(index, value) {
		if (index == 0) {
			$('#account-list >li.' + value).prependTo('#account-list').addClass('enabled-account').removeClass('disabled-account').find('.progress').removeClass('hide');
		} else {
			$('#account-list >li.' + value).insertAfter($('#account-list > li:eq(' + (index - 1) + ')')).addClass('enabled-account').removeClass('disabled-account').find('.progress').removeClass('hide');
		}
	});
	$('#account-list').sortable({
		axis : 'y',
		stop : function(event, ui) {
			savePriority();
		}
	});

	$('#account-list > li').bind({
		click : function() {
			$('#account-list > li').removeClass('selected');
			var acc = $(this);
			acc.addClass('selected');
			if (acc.hasClass('enabled-account')) {
				$(acc.attr('href') + '> .account-add').addClass('hide').find('button.authorize').unbind();
				$(acc.attr('href') + '> .account-summary').removeClass('hide').find('button.deauthorize').unbind().bind({
					click : function(e) {
						var srv = $(this);
						BEAM.log("Selected:" + srv.attr('beam-service'));
						if (acc.hasClass('enabled-account') && srv.hasClass('deauthorize')) {
							BeamStorageManager.setCollection(BeamStorageCollections[srv.attr('beam-service').toUpperCase() + '_COLLECTION_KEY'], {});
							acc.removeClass('enabled-account').addClass('disabled-account');
							savePriority();
							acc.click();
						}
						// $(this).toggleClass('disabled-account');

					}
				});

			} else {
				$(acc.attr('href') + '> .account-add').removeClass('hide').find('button.authorize').unbind().bind({
					click : function(e) {
						var srv = $(this);
						BEAM.log("Selected:" + srv.attr('beam-service'));
						if (!(acc.hasClass('enabled-account')) && srv.hasClass('authorize')) {
							BeamCloudManager[srv.attr('beam-service')].getAccInfo(function(userDet) {
								acc.removeClass('disabled-account');
								acc.addClass('enabled-account');
								// info.find();
								acc.find('.progress').removeClass('hide');								acc.find('.progress > .bar').width( userDet.per + '%');
								acc.click();
								savePriority();
							}, function() {
								acc.removeClass('enabled-account');
								acc.addClass('disabled-account');
								acc.find('.progress').addClass('hide');
							});
						}
					}
				});
				$(acc.attr('href') + '> .account-summary').addClass('hide').find('button.deauthorize').unbind();
			}
		},	});
	
	savePriority();
	$('#account-list > li:first()').click();
});
