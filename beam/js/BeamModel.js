/*
 * All the UI related javascript function definitions are here
 */
var navHistoryStack = new Array();
var currentNavigator = null;

function renderFilesHTML(files, onlyImage) {
	var html = "";
	var count = files.length;

	for (var i = 0; i < count; i++) {
		var obj = files[i];
		var icon;
		var filefolderclass = "desktop-file";
		switch(obj.type) {

			// File types
			case 'video/mpeg' :
			case 'video/mp4':
			case 'video/ogg':
			case 'video/quicktime':
			case 'video/webm':
			case 'video/x-matroska':
			case 'video/x-ms-wmv':
			case 'video/x-flv':
			case 'video'   :
				iconImageSource = 'mimetypes/video-x-generic.png';
				break;
				
			case 'audio'   :
			case 'audio/basic'   :
			case 'audio/L24'   :
			case 'audio/mp4'   :
			case 'audio/mpeg'   :
			case 'audio/ogg'   :
			case 'audio/vorbis'   :
			case 'audio/vnd.rn-realaudio'   :
			case 'audio/vnd.wave'   :
			case 'audio/webm'   :
				iconImageSource = 'mimetypes/audio-x-generic.png';
				break;

			case 'image/gif':
			case 'image/jpeg':
			case 'image/pjpeg':
			case 'image/png':
			case 'image/svg+xml':
			case 'image/tiff':
			case 'image/vnd.microsoft.icon':
			case 'image'     :
				iconImageSource = 'mimetypes/image-x-generic.png';
				break;

			case 'favorite':
				iconImageSource = 'places/favorites.png';
				break;
			case 'bookmark':
				iconImageSource = 'places/bookmarks.png';
				break;

			case 'link'    :
				iconImageSource = 'mimetypes/text-html.png';
				break;
			case 'email'   :
				iconImageSource = 'places/mail-message.png';
				break;
			case 'file'    :
				iconImageSource = 'mimetypes/x-office-document.png';
				break;
			case 'unknown' :
				iconImageSource = 'mimetypes/unknown.png';
				break;

			case 'text'    :
				iconImageSource = 'mimetypes/text-plain.png';
				break;
			case 'pdf'     :
				iconImageSource = 'mimetypes/application-pdf.png';
				break;
			case 'archive' :
				iconImageSource = 'mimetypes/application-x-archive.png';
				break;
			case 'app'     :
				iconImageSource = 'places/start-here.png';
				break;

			default        :
				iconImageSource = 'mimetypes/unknown.png';
				break;

			// Folder types
			case 'folder'  :
				switch(obj.title) {
					case 'Music':
						iconImageSource = 'places/folder-sound.png';
						break;
					case 'Videos':
						iconImageSource = 'places/folder-video.png';
						break;
					case 'Images':
						iconImageSource = 'places/folder-image.png';
						break;
					case 'Documents':
						iconImageSource = 'places/folder-documents.png';
						break;
					case 'Bookmarks':
						iconImageSource = 'places/folder-remote.png';
						break;
					default :
						iconImageSource = 'mimetypes/inode-directory.png';
						break;
				}
				filefolderclass = "desktop-folder";
				break;

			case 'drive'   :
				iconImageSource = 'devices/drive-harddisk.png';
				filefolderclass = "desktop-folder";
				break;

		}

		if (!!!onlyImage) {
			html += Handlebars.templates.DesktopIcon({
				beamId : obj.beamId,
				title : obj.title,
				iconImage : iconImageSource,
				fileFolderClass : filefolderclass,
				directory : obj.dir
			});
		} else {
			html += '<li directory=' + obj.dir + '>' + Handlebars.templates.DesktopIcon({
				title : obj.title,
				iconImage : iconImageSource,
				fileFolderClass : filefolderclass,
			}) + '</li>';
		}

	}

	return html;
}

function refreshFS() {
	var cloudPriority = BeamStorageManager.getCollection(BeamStorageCollections.CLOUD_PRIORITY_KEY);
	$.each(cloudPriority, function(k, v) {
		BeamCloudManager[v].refresh();
	});
}

function renderDesktop() {

	BEAM.log("Rendering Desktop...");

	/* Creating Desktop Folder and Music Folder */
	var BFM = new BeamFileManager();
	BFM.initFS(function() {
		BFM.changeDirectory('/');
		getFormattedDirectoryListing(BFM, function(response) {
			$('#desktop').prepend(renderFilesHTML(response));
		});
	});
}

function renderNavigator(scandir, nav) {
	$('#desktop').prepend(Handlebars.templates.BasicWindow({
		beamId : nav.beamId,
		topTitle : nav.title,
		hasSideBar : false,
		isHTML : true,
		html : renderFilesHTML(scandir)
	}));
	$('[id="win-' + nav.beamId + '"]').show('fast');
	BeamDesktop.bringWindowToFront(nav.beamId);
}

function getFormattedDirectoryListing(BFM, success) {
	BEAM.log("Getting formatted directory listing for beamid:" + BFM.currentDirectory());
	BFM.list(function(entries) {
		// if (entries.length) {
		var n = entries.length;
		var formatted = new Array();
		$.each(entries, function(key, file) {
			formatted.push({
				title : file.name,
				type : BeamStorageManager.getFileInfo(file.fullPath, 'type'),
				beamId : file.fullPath,
				dir : file.isDirectory
			});
		});
		success(formatted);
		// }
	});
}

function openNavigator(BFM, title) {
	BEAM.log("Opening Navigator with title:" + title);

	if (currentNavigator)
		navHistoryStack.push(currentNavigator);

	getFormattedDirectoryListing(BFM, function(scandir) {
		console.log(scandir);
		currentNavigator = {
			beamId : BFM.currentDirectory(),
			title : title
		};
		renderNavigator(scandir, currentNavigator);
	});

}

function backNavigator() {
	if (navHistoryStack.length > 0) {
		nav = navHistoryStack.pop();
		currentNavigator = null;
		openNavigator(nav.beamId, nav.title);
	}
}

function resetNavigator() {
	navHistoryStack = new Array();
	currentNavigator = null;
}

function openFile(path, title) {
	BEAM.log("Opening file with path:" + path);
	var bfm = new BeamFileManager();
	bfm.initFS(function(dir) {
		bfm.open(path, function(fsUrl) {
			console.log(path);
			$('#desktop').prepend(Handlebars.templates.BasicWindow({
				beamId : path,
				topTitle : title,
				hasSideBar : false,
				isHTML : true,
				html : (function() {
					return $('<div></div>').append($('<iframe></iframe>').attr('src', fsUrl).attr('height', '100%').attr('width', '100%')).html();
				})()
			}));
			$('[id="win-' + path + '"]').show('fast');
			BeamDesktop.bringWindowToFront(path);
		});
	});
}

function savePriority() {
	BEAM.log("Saving service priority...");
	var priority = {};
	$('#account-list > li.disabled-account > .progress').addClass('hide');
	$('#account-list > li.enabled-account').each(function(index) {
		var acc = $(this);
		BeamCloudManager[acc.attr('beam-service')].getAccInfo(function(userDet) {
			acc.find('.progress > .bar').width(userDet.per + '%');
			acc.find('.progress').removeClass('hide');
		});
		priority[index] = acc.attr('beam-service');
	});
	BeamStorageManager.setCollection(BeamStorageCollections.CLOUD_PRIORITY_KEY, priority);
}