$('#beamos-login-btn').bind({
		click : function() {
			$('#login-modal').modal('show');
		}	
});

$('#beamos-logout-btn').bind({
		click : function() {
			$.ajax({
				type:"GET",
				url:"http://beamos.arkene.com/beamos/logout",
				success:function(json) {
					var data = eval(json);
					$('#beamos-logout-btn').hide();
					$('#beamos-login-btn').show();
					console.log(data);
				},
				error:function() {

				}
			}); 
		}
});

$('#beamos-google-login-btn').bind({
	click: function() {
		window.open("http://beamos.arkene.com/auth/google","_self");
	}
});

//Check login status
$.ajax({
		type:"GET",
		url:"http://beamos.arkene.com/beamos/login/status",
		success:function(json) {
			var data = eval(json);
			if(data["status"]==0) {
				if(data["loginStatus"]) {
					$('#beamos-logout-btn').show();
					$('#beamos-login-btn').hide();
				} else {
					$('#beamos-logout-btn').hide();
					$('#beamos-login-btn').show();	
				}
			} else {
				//Server error
				//NOTIFY
			}
			console.log(data);
		},
			error:function() {
		}	
}); 