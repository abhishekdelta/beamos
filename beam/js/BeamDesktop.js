/*
 * Beam Desktop
 */

var BeamDesktop = {

	weekdays : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	months : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

	activeWindows : [],

	init : function() {
		this.breakFrames();
		this.updateClock();
		//refresh at every 15 minutes
		setInterval(refreshFS,900000);
		renderDesktop();
	},

	breakFrames : function() {
		if (window.location !== window.top.location) {
			window.top.location = window.location;
		}
	},

	showMenu : function(menuHeadObj) {
		$(menuHeadObj).addClass('active').next('ul.menu').show();
	},

	highlightDesktopIcon : function(iconObj) {
		$(iconObj).addClass('active');
	},

	closeMenus : function() {
		this.hideContextMenu();
		$('a.active, div.active, tr.active').removeClass('active');
		$('ul.menu').hide();
	},

	showContextMenu : function() {
		console.log("Context menu is supposed to be open now.")
		//@TODO
	},

	hideContextMenu : function() {
		console.log("Context menu is supposed to be hidden now.")
		//@TODO
	},

	bringWindowToFront : function(beamId) {
		$('div.window').removeClass('window-stack');
		$('[id="win-' + beamId + '"]').addClass('window-stack').show();
	},

	minimizeAllWindows : function() {
		$('div.window').hide();
	},

	showAllWindows : function() {
		$('div.window').show();
	},

	minimizeWindow : function(beamId) {
		$('[id="win-' + beamId + '"]').hide();
	},

	closeWindow : function(beamId) {
		var index = this.activeWindows.indexOf(beamId);
		if (index != -1) {
			this.activeWindows.splice(index, 1);
		}
		$('[id="win-' + beamId + '"]').remove();
		$('[id="dock-' + beamId + '"]').remove();
	},

	resizeWindow : function(beamId) {
		var win = $('[id="win-' + beamId + '"]');

		// If already full-sized then restore to original size
		// else save window original dimension and maximize it
		if (win.hasClass('window-full')) {
			win.removeClass('window-full').css({
				'top' : win.attr('data-beam-win-t'),
				'left' : win.attr('data-beam-win-l'),
				'right' : win.attr('data-beam-win-r'),
				'bottom' : win.attr('data-beam-win-b'),
				'width' : win.attr('data-beam-win-w'),
				'height' : win.attr('data-beam-win-h')
			});
		} else {
			win.attr({
				'data-beam-win-t' : win.css('top'),
				'data-beam-win-l' : win.css('left'),
				'data-beam-win-r' : win.css('right'),
				'data-beam-win-b' : win.css('bottom'),
				'data-beam-win-w' : win.css('width'),
				'data-beam-win-h' : win.css('height')
			}).addClass('window-full').css({
				'top' : '0',
				'left' : '0',
				'right' : '0',
				'bottom' : '0',
				'width' : '100%',
				'height' : '100%'
			});
		}

		this.bringWindowToFront(beamId);
	},

	activateTaskbarIcon : function(iconObj) {
		var beamId = $(iconObj).attr('data-beam-id');
		// Make sure its in the activeWindows list, else remove it
		if (this.activeWindows.indexOf(beamId) != -1) {
			var win = $('[id="win-' + beamId + '"]');

			// Hide, if window is visible (even if in back) else bring it to front
			if (win.is(':visible')) {
				win.hide();
			} else {
				this.bringWindowToFront(beamId);
			}
		} else {
			$(iconObj).remove();
		}
	},

	activateDesktopIcon : function(iconObj) {

		var icon = $(iconObj);
		var title = icon.attr('title');
		var beamId = icon.attr('data-beam-id');
		var imgSrc = icon.find('img').attr('src');
		var dockId = 'dock-' + beamId;

		// If window is not open, add it to the bottom dock and handle its opening.
		// If its already open, simply bring it to front.
		if (this.activeWindows.indexOf(beamId) == -1) {
			this.activeWindows.push(beamId);

			// Create the dock element, place it on the dock and show it
			$('#dock').append(Handlebars.templates.DockItem({
				beamId : beamId,
				imgSrc : imgSrc,
				title : title
			}));

			$('[id="' + dockId + '"]').show('fast');

			// Determine its a file or folder and handle appropriately
			if (icon.find('div.desktop-icon').hasClass('desktop-folder')) {
				var bfm = new BeamFileManager();
				bfm.initFS(function() {
					bfm.changeDirectory(beamId, function() {
						openNavigator(bfm, title);
					});

				});
			} else {
				openFile(beamId, title);
			}
		} else {
			this.bringWindowToFront(beamId);
		}
	},

	updateClock : function() {
		var clock = $('#clock');

		if (!clock.length) {
			return;
		}

		var date_obj = new Date();

		weekday = this.weekdays[date_obj.getDay()];
		month = this.months[date_obj.getMonth()];

		var hour = date_obj.getHours();
		var minute = date_obj.getMinutes();
		var day = date_obj.getDate();
		var year = date_obj.getFullYear();

		var suffix = 'AM';
		if (hour >= 12) {
			suffix = 'PM';
		}

		if (hour > 12) {
			hour = hour - 12;
		} else if (hour === 0) {
			// Display 12:XX instead of 0:XX.
			hour = 12;
		}

		// Leading zero, if needed.
		if (minute < 10) {
			minute = '0' + minute;
		}

		var clock_time = weekday + ' ' + hour + ':' + minute + ' ' + suffix;
		var clock_date = month + ' ' + day + ', ' + year;
		clock.html(clock_time).attr('title', clock_date);

		setTimeout(this.updateClock, 60000);
	}
};
