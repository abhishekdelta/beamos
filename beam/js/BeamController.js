/*
 * All the UI related javascript event bindings are here. Entry point of Javascript.
 */
 
$(document).ready(function() {
	
	BEAM.log("Initializing BEAM ...");
	
	var status = BEAM.init();
	
	// If initialization is not successful, ask the user to goto Settings page first.
	if(!status) {
		$('#settings-redirect-btn').click(function(){
			location.href = chrome.extension.getURL('settings.html');
		});
		$('#settings-modal').modal({
			keyboard: false,
			backdrop: 'static',
			show: true,
		});
		return;
	}
	else {
		BeamDesktop.init();
	}
    
	var body = $('body');
	var document = $(this);

	// Make icons draggable
    document.on('mouseenter', 'div.draggable', function() {
      $(this).off('mouseenter').draggable({
        containment: 'parent'
      });
    });    

    // Make windows draggable
    document.on('mouseenter', 'div.window', function() {
      $(this).off('mouseenter').draggable({
        // Confine to desktop.
        // Movable via top bar only.
        cancel: 'a',
        containment: 'parent',
        handle: 'div.window-top'
      }).resizable({
        containment: 'parent',
        minWidth: 400,
        minHeight: 200
      });
    });

    // Cancel single-click on desktop icons and instead just highlight them.
	document.on('mousedown', '.icon', function() {
        BeamDesktop.highlightDesktopIcon(this);    	
    });

    // Respond to double-click on desktop icons
	document.on('dblclick', '.icon', function() {
	    BeamDesktop.activateDesktopIcon(this);
    });


    // Double-click top bar to resize
	document.on('dblclick', 'div.window-top', function() {
		BeamDesktop.resizeWindow($(this).attr('data-beam-id'));
    });

    // Activate minimize button
	document.on('click', 'a.window-min', function() {
    	BeamDesktop.minimizeWindow($(this).attr('data-beam-id'));     
    });
    
    // Activate maximize button
	document.on('click', 'a.window-resize', function() {
    	BeamDesktop.resizeWindow($(this).attr('data-beam-id'));     
    });

    // Activate close button
	document.on('click', 'a.window-close', function() {
    	BeamDesktop.closeWindow($(this).attr('data-beam-id'));      
    });

	// Activate show desktop button
    document.on('mousedown', '#show-desktop', function() {

      // Hide all visible windows
      if ($('div.window:visible').length) {
    	  BeamDesktop.minimizeAllWindows();       
      }
      else {
    	  BeamDesktop.showAllWindows();            
      }
    });

	
    // Activate bottom taskbar dock buttons
    document.on('click', '#dock li', function() {
    	BeamDesktop.activateTaskbarIcon(this);
    });
    
    // Focus active window
    document.on('mousedown', 'div.window', function() { 
      BeamDesktop.bringWindowToFront($(this).attr('data-beam-id'));      
    });
    
    // Any remote links are supposed to open in a new tab
    document.on('click', 'a', function(e) {    
      var url = $(this).attr('href');
      this.blur();
      // Relative links are not allowed
      if (url && url.match(/^#/)) {
        e.preventDefault();
        e.stopPropagation();
      }
      else if(url) {
        $(this).attr('target', '_blank');
      }
    });
    
    // Cancel right-click, and display BEAM Desktop's context menu instead.
    document.on('contextmenu', function() {
    	BeamDesktop.showContextMenu();
    	return false;
    });
    
	// When someone does a mousedown anywhere, all menus are closed
	// Just like how any OS behaves. 
    document.mousedown(function(e) {    	    
    	// Excluding the places where a click is actually required
        var tags = 'a, button, input, select, textarea, tr';
        if (!$(e.target).closest(tags).length) {
          BeamDesktop.closeMenus();
          e.preventDefault();
          e.stopPropagation();
        }
    });
	        
    // Activate top menu bar
    document.on('mousedown', 'a.menu-head', function() {
      if ($(this).next('ul.menu').is(':hidden')) {
    	BeamDesktop.closeMenus();
    	BeamDesktop.showMenu(this);        
      }
      else {
    	BeamDesktop.closeMenus();
      }
    });

    // If any of the top menu is open already, then simply transfer focus.
    document.on('mouseenter', 'a.menu-head', function() {      
      if ($('ul.menu').is(':visible')) {
    	BeamDesktop.closeMenus();
      	BeamDesktop.showMenu(this);           
      }
    });

});
