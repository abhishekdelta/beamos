/*
 * Beam OS Settings Page entry-point
 */

$(document).ready(function() {

	BEAM.log("Initializing BEAM for User Input Page.");

	BEAM.init(true);

	$('#file-list').sortable({
		stop : function(event, ui) {
			// savePriority();
			// function to change order
		}
	});

	var _INVALID_PATH_NAME_PATTERN = /[\x00-\x1f\x7f:;*?\\/"<>|]/;
	var FSN = function() {

	};
	FSN.contentWindow = (function() {
		return {
			postMessage : function(msg, loc) {
				switch(msg.cmd) {
					case 'mkdir':
					//add new directory entry
					// $('#file-list').prepend(generatedentry);
					case 'cd':
					//refresh the view

					case 'rm':
					//remove the file or directory from the view
					//searh for the file/folder and delete it
					case 'touch':
						//add the fileentry
						getFormattedDirectoryListing(BFM, function(response) {
							$('#file-list').html(renderFilesHTML(response, true));
							$('#upload-bc').html(BFM.currentDirectory().replace(/\//gi, '>'));
							$('#file-list > li[directory=true] > div').unbind().bind({
								click : function(e) {
									BFM.changeDirectory($(this).attr('title'));
								}
							});
							$('#file-list > li[directory=false] > div').unbind().bind({
								click : function(e) {
									$('#file-name').val($(this).attr('title'));
								}
							});
						});
						break;
					case 'error':
						if (msg.data.code == FileError.NOT_FOUND_ERR) {
							BEAM.error(msg.data.error.dest + ': No such file or directory');
						} else if (msg.data.code == FileError.INVALID_STATE_ERR) {
							BEAM.error(msg.data.error.dest + ': Not a directory');
						} else if (msg.data.error.code == FileError.INVALID_MODIFICATION_ERR) {
							BEAM.error(msg.data.dest + ': File already exists');
						}
						break;
				}
			}
		};
	})();
	var BFM = new BeamFileManager(FSN);
	var showFiles = function(dirPath) {
		if (!!!dirPath) {
			dirPath = '/';
		}
		//return current directory and list of files
		BFM.changeDirectory('/');
	};

	info = null;
	tab = null;
	setDetails = function(info_, tab_) {
		BEAM.log('Setting file details');
		console.log(info_, tab_);
		info = info_;
		tab = tab_;
	}
	BFM.initFS(showFiles);
	$('#file-action > li > a').bind({
		click : function() {
			var action = $(this);

			switch(action.attr('beam-action')) {
				case 'up' :
					BFM.changeDirectory('../');
					break;
				case 'new' :
					var folderName = prompt('Enter folder name.');
					if (folderName.match(_INVALID_PATH_NAME_PATTERN)) {
						BEAM.alert("Error: Invalid character in folder name.");
						action.click();
						break;
					}
					BFM.createDirectory(folderName);
					break;
			}
		},
	});
	console.log(info);
	console.log(tab);
	$('#file-data > form').bind({
		submit : function(e) {

			var onLocalSaveSuccess = function() {
				BEAM.log('On Local Save Success');
				window.close();			}
			// console.log(info);
			// console.log(tab);
			//enable Uploading

			var fileName = $('#file-name').val();

			if (fileName.match(_INVALID_PATH_NAME_PATTERN)) {
				BEAM.alert("Error: Invalid character in file name.");
				$('#file-name').val('')
				e.preventDefault();
				return false;
			}

			$('#upload-modal').modal({
				keyboard : false,
				backdrop : 'static',
				show : true,
			});

			if (info.selectionText) {
				BEAM.log('Saving selected text');
				BFM.save(info.selectionText, "text", fileName, onLocalSaveSuccess);
			} else if (info.linkUrl) {
				BEAM.log('Downloading file at ' + info.linkUrl);
				BFM.downloadAndSave(info.linkUrl, fileName, onLocalSaveSuccess);
			} else if (info.mediaType) {
				BEAM.log('Downloading file from ' + info.srcUrl);
				BFM.downloadAndSave(info.srcUrl, fileName, onLocalSaveSuccess);
			} else {
				pageUrl = info.pageUrl;
				// Special handling for Youtube videos
				matches = pageUrl.match(/(?:http:\/\/)?(?:(?:www\.)?youtube\.com\/watch\?)(?:.*)v=([a-zA-Z0-9_-]*)/);
				if (matches != null && matches.length > 0) {
					BFM.save(matches[1], "link", fileName, onLocalSaveSuccess);
					BEAM.log("Youtube video:" + matches[1]);
				} else {
					//have to figure what to do if none of the above is true
				}
			}
			e.preventDefault();
			return false;
		}
	});
});
