/**
 * @author Akash Chauhan <akash [at] gmail [dot] com
 */

var BeamSkydriveManager = function() {
};

BEAM.extend(BeamSkydriveManager, BeamCloudInterface);

BeamSkydriveManager.prototype.getAuthToken = function() {
    //check if auth_token is present
    //if yes than return the token else generate one

    if (BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token') == undefined) {
        var cxAuthWindowId;
        chrome.windows.create({
            url : 'https://www.cx.com/mycx/oauth/authorize?client_id=' + BeamConfig.CX_KEY + '&redirect_uri=https://tranquil-citadel-6176.herokuapp.com/redirect_uri/cx',
            width : 640,
            height : 480,
            focused : true,
            type : "popup"
        }, function(window) {
            cxAuthWindowId = window.id;
        });
        chrome.extension.onConnect.addListener(function(port) {
            if (port.name == 'cxPort') {
                port.onMessage.addListener(function(msg) {
                    if (msg != null) {
                        BeamStorageManager.insertIntoCollection(BeamStorageCollections.CX_COLLECTION_KEY, "access_token", msg.access_token);
                        BEAM.log('CX Access_Token Stored');
                    } else {
                        BEAM.error('User denied access to CX account');
                    }
                    chrome.windows.remove(cxAuthWindowId);
                    BEAM.log('CX Auth Window closed');
                    return BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token');
                });
            }
        });
    } else {
        return BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token');
    };
};

/**
 *
 * @param String type  can be api/data
 * @param String resource
 * @param String method (optional)
 * @param String params (optional)
 * @param String api (optional) defaults to 1
 */
BeamSkydriveManager.prototype.generateURL = function(type, resource, method, params, api) {
    params = $.extend({
        access_token : BeamCloudManager.cx.getAuthToken()
    }, params);
    api = ((api == undefined) ? '1/' : (api + '/'));
    resource = ((resource == undefined) ? '' : (resource + '/'));
    method = ((method == undefined) ? '' : method);
    params = ((params == undefined) ? '' : ('?' + $.param(params)));

    return 'https://' + type + '.cx.com/' + api + resource + method + params;
}

BeamSkydriveManager.prototype.uploadFile = function(filePath, contents, success, error) {
/*
 * Request Type    POST    
 * Resource        data
 * Path Param      path                This is File Path that specifies the upload destination
 * Querey Param    access_token    
 *                 client_id           Your mashery Client_ID for verification
 *                 content_md5         Optional Url-Safe encoded Base64 String - Highly Recommended
 *                 content_sha1        Optional Url-Safe encoded Base64 String - Highly Recommended
 *                 file_size           Required for files over 10mb. Highly recommended otherwise.
 *                 file_content_type   MIME Type of the file being uploaded
 * Form Param      file                The file data (raw bytes). 
 * 
 * https://data.cx.com/1/data/self:/file1.jpg?accessToken=MySecretToken
 */

    
};

BeamSkydriveManager.prototype.readFile = function(filename, success, error) {
};

BeamSkydriveManager.prototype.downloadFileLink = function(filePath, success, error) {
    //https://data.cx.com/1/data/self:/Test/AkashCV.doc?access_token=2nbjraqwa8w7abvn2e4bv5k8&client_id=q88rupr527bp7ywzfjysfuey
    if (filePath == undefined) {
        error();
    } else {
        var fileLink = BeamCloudManager.cx.generateURL('data', 'data', 'self:/' + encodeURIComponent(filePath) + '/', {
            client_id : BeamConfig.CX_KEY
        });
        success(fileLink);
    }

};

BeamSkydriveManager.prototype.getSpaceRemaining = function(sdSuccess, sdError) {
    //https://api.cx.com/1/users/self?access_token=${token}
    $.ajax({
        url : BeamCloudManager.cx.generateURL('api', 'users', 'self'),
        dataType : "json",
        success : function(response) {
            if (response.hasOwnProperty('profile')) {
                cxSuccess(response.profile.quota.totalCapacity - response.profile.quota.totalConsumed);
            }else{
                cxError();
            }
        },
        error : cxError
    });
};

BeamSkydriveManager.prototype.getFileList = function(path, sdSuccess, sdError) {
    // https://api.cx.com/1/files/list/self:/?access_token=MySecretToke
    if (path == undefined || path == null) {
        path = 'self:/';
    } else {
        path = 'self:/' + encodeURIComponent(path) + '/';
    }
    $.ajax({
        url : BeamCloudManager.cx.generateURL('api', 'files', 'list/' + path),
        dataType : "json",
        success : cxSuccess,
        error : cxError
    });
};
