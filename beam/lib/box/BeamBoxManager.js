/**
 * @author Akash Chauhan <akash [at] gmail [dot] com
 */

var BeamBoxManager = function() {
};

BEAM.extend(BeamBoxManager, BeamCloudInterface);

BeamBoxManager.prototype.getAuthToken = function(callback) {
	//check if auth_token is present
	//if yes than return the token else generate one

	BEAM.log('Fetching Auth Token');
	var auth_token = BeamStorageManager.findInCollection(BeamStorageCollections.BOX_COLLECTION_KEY, 'access_token');
	if (auth_token == undefined) {
		//get ticket by https://www.box.com/api/1.0/rest?action=get_ticket&api_key={your api key}
		$.ajax({
			url : 'https://www.box.com/api/1.0/rest?action=get_ticket&api_key=' + BeamConfig.BOX_KEY,
			type : 'GET',
			dataType : 'xml',
			success : function(xml) {
				var ticket = null;
				if ($(xml).find('status').text() == 'get_ticket_ok') {
					ticket = $(xml).find('ticket').text();
					var boxAuthWindowId;
					chrome.windows.create({
						url : 'https://m.box.com/api/1.0/auth/' + ticket,
						width : 640,
						height : 640,
						focused : true,
						type : "popup"
					}, function(window) {
						boxAuthWindowId = window.id;
					});
					chrome.extension.onConnect.addListener(function(port) {
						if (port.name == 'boxPort') {
							port.onMessage.addListener(function(msg) {
								if (msg == 'get_auth_token') {
									$.ajax({
										url : 'https://m.box.com/api/1.0/rest?action=get_auth_token&api_key=' + BeamConfig.BOX_KEY + '&ticket=' + ticket,
										dataType : 'xml',
										success : function(xmlRes) {
											if ($(xmlRes).find('status').text() == 'get_auth_token_ok') {
												BeamStorageManager.insertIntoCollection(BeamStorageCollections.BOX_COLLECTION_KEY, "access_token", $(xmlRes).find('auth_token').text());
												BEAM.log('BOX Access_Token Stored');
												if (callback != undefined)
													callback(BeamStorageManager.findInCollection(BeamStorageCollections.BOX_COLLECTION_KEY, 'access_token'));
												return BeamStorageManager.findInCollection(BeamStorageCollections.BOX_COLLECTION_KEY, 'access_token');
												return;
											} else {
												BEAM.error('Oops some error while authentication, please try again');
											}
										}
									});

								} else {
									BEAM.error('User denied access to BOX account');
								}
								chrome.windows.remove(boxAuthWindowId);
								BEAM.log('BOX Auth Window closed');

							});
						}
					});
				} else {
					BEAM.error('Oops some error in accessing box account, please try again after some time.');
				}
			},
			error : function() {
				BEAM.error('Oops some error in accessing box account, please try again after some time.')
			}
		});
		//open window with https://www.box.com/api/1.0/auth/{your ticket}

	} else {
		if (callback != undefined) {
			callback(auth_token);
		}
		return auth_token;
	};
};

/**
 *
 * @param String type  can be api/www
 * @param String resource
 * @param String method (optional)
 * @param String params (optional)
 * @param String api (optional) defaults to 2
 */
BeamBoxManager.prototype.generateURL = function(type, resource, method, params, api) {
	if (api == undefined) {
		api = '2.0/';
	} else {
		api += '.0/'
		params = $.extend({
			api_key : BeamConfig.BOX_KEY,
			auth_token : BeamCloudManager.box.getAuthToken()
		}, params);
	}
	resource = ((resource == undefined) ? '' : (resource + '/'));
	method = ((method == undefined) ? '' : method);
	params = ((params == undefined) ? '' : ('?' + $.param(params)));

	return 'https://' + type + '.box.com/' + api + resource + method + params;
};

/**
 * @param Object an xhr
 * @desc  To set BoxAuth Header
 */
BeamBoxManager.prototype.headers = function() {
	return {
		'Authorization' : 'BoxAuth api_key=' + BeamConfig.BOX_KEY + '&auth_token=' + BeamCloudManager.box.getAuthToken() ,
	};
};

BeamBoxManager.prototype.uploadFile = function(path, contents, boxSuccess, boxError) {
	folderId = '0';
	if (folderId == undefined) {
		boxError();
	} else {
		var fileLink = BeamCloudManager.box.generateURL('api', 'files', 'content');
		var fData = new FormData();
		fData.append('file', contents, path);
		fData.append('folder_id', folderId);

		$.ajax({
			url : fileLink,
			data : fData,
			type : 'POST',
			contentType : false,
			processData : false,
			dataType : 'json',
			headers : BeamCloudManager.box.headers(),
			success : function(response) {
				// console.log(response);				boxSuccess(response.id);			},
			error : boxError
		});
	}
};

BeamBoxManager.prototype.readFile = function(filename, success, error) {
	// GET /files/{file id}/content
};

BeamBoxManager.prototype.downloadFileLink = function(filePath, success, error) {

};

BeamBoxManager.prototype.getSpaceRemaining = function(boxSuccess, boxError) {
	//https://api.box.com/1.0/rest
	BEAM.log('Calculating Space Remaining');
	$.ajax({
		url : BeamCloudManager.box.generateURL('api', '', 'rest', {
			'action' : 'get_account_info',
			'api_key' : BeamConfig.BOX_KEY
		}, '1'),
		dataType : "xml",
		success : function(response) {
			if ($(response).find('status').text() == 'get_account_info_ok') {
				boxSuccess(parseInt($(response).find('user > space_amount').text()) - parseInt($(response).find('user > space_used').text()));
			} else {
				BEAM.error('Unable to get box account info');
				boxError();
			}
		},
		error : boxError
	});
};

BeamBoxManager.prototype.getFileList = function(folderId, boxSuccess, boxError) {
	// https://api.box.com/2.0/folders/FOLDER_ID/items
	folderId = (folderId == undefined) ? '0' : folderId;
	$.ajax({
		url : BeamCloudManager.box.generateURL('api', 'folders', folderId + '/items'),
		headers : BeamCloudManager.box.headers(),
		dataType : "json",
		success : boxSuccess,
		error : boxError
	});
};

BeamBoxManager.prototype.refresh = function(boxSuccess, boxError) {
	// GET /events?stream_position=0	// https://api.box.com/2.0/events?stream_position=0
	var streamPosition = BeamStorageManager.findInCollection(BeamStorageCollections.BOX_COLLECTION_KEY, 'stream_position');

	$.ajax({
		url : BeamCloudManager.box.generateURL('api', 'events', '', {
			'stream_position' : streamPosition,
			'stream_type' : 'changes'
		}),
		headers : BeamCloudManager.box.headers(),
		dataType : "json",
		success : function(response) {
			//process entries and create the same in the application
			var entries = response.entries;
			console.log(entries);
			/**
			 * Possible Events by the user
			 *+++++++++++++++++++++++++++++
			 *
			 * ITEM_CREATE					A folder or File was created
			 * ITEM_UPLOAD					A folder or File was uploaded
			 * COMMENT_CREATE				A comment was created on a folder, file, discussion, or other comment
			 * ITEM_DOWNLOAD				A file or folder was downloaded
			 * ITEM_PREVIEW					A file was previewed
			 * ITEM_MOVE					A file or folder was moved
			 * ITEM_COPY					A file or folder was copied
			 * TASK_ASSIGNMENT_CREATE		A task was assigned
			 * LOCK_CREATE					A file was locked
			 * LOCK_DESTROY					A file was unlocked
			 * ITEM_TRASH					A file or folder was marked as deleted
			 * ITEM_UNDELETE_VIA_TRASH		A file or folder was recovered out of the trash
			 * COLLAB_ADD_COLLABORATOR		A collaborator was added to a folder
			 * COLLAB_INVITE_COLLABORATOR	A collaborator was invited on a folder
			 * ITEM_SYNC					A folder was marked for sync
			 * ITEM_UNSYNC					A folder was un-marked for sync
			 * ITEM_RENAME					A file or folder was renamed
			 * ITEM_SHARED_CREATE			A file or folder was enabled for sharing
			 * ITEM_SHARED_UNSHARE			A file or folder was disabled for sharing
			 * ITEM_SHARED					A folder was shared
			 * TAG_ITEM_CREATE				A Tag was added to a file or folder
			 */
			//create changes in the file system according to your entries
			var BFM = new BeamFileManager();
			BFM.initFS(function(dirPath) {
				entries.forEach(function(value,key) {
					switch(value.event_type) {
						case 'ITEM_CREATE':
							BEAM.log('A '+value.source.type+' was created under ' + value.source.parent.id);
							//seach for parentId and create this entry there
							//create the path
							//update the id's
							break;
						case 'ITEM_UPLOAD':
							BEAM.log('A '+value.source.type+' was uploaded at ' + value.source.path);
							//create the path and update the id's
							
							break;
						case 'ITEM_DOWNLOAD':
							BEAM.log('A file or folder was downloaded from ' + value.source.path);
							//don't do anything
							break;
						case 'ITEM_MOVE':
							BEAM.log('A '+value.source.type+' was moved');
							//change the path
							//get current path of the id 
							//create under new parent id and delete from cuurent recursively path
							break;
						case 'ITEM_COPY':
							BEAM.log('A '+value.source.type+' was copied');
							//add a new path
							break;
						case 'ITEM_TRASH':
							BEAM.log('A '+value.source.type+' was marked as deleted');
							//move item to trash
							break;
						case 'ITEM_UNDELETE_VIA_TRASH':
							BEAM.log('A '+value.source.type+' was recovered out of the trash');
							//unmove item from trash ..create the item again
							break;
						case 'ITEM_RENAME':
							BEAM.log('A '+value.source.type+' was renamed');
							//move item
							break;
					}

				});
				// BeamStorageManager.insertIntoCollection(BeamStorageCollections.BOX_COLLECTION_KEY,'stream_position',response.next_stream_position);
			});
			// console.log(response);			boxSuccess(response);
		},
		error : boxError
	});
};
BeamBoxManager.prototype.getAccInfo = function(boxSuccess, boxError) {
	//https://api.cx.com/1/users/self?access_token=${token}
	BEAM.log('Calculating Space Remaining');
	BeamCloudManager.box.getAuthToken(function() {
		BEAM.log("Auth token recieved");
		$.ajax({
			url : BeamCloudManager.box.generateURL('api', '', 'rest', {
				'action' : 'get_account_info',
				'api_key' : BeamConfig.BOX_KEY
			}, '1'),
			dataType : "xml",
			success : function(response) {
				if ($(response).find('status').text() == 'get_account_info_ok') {
					var totalSpace = parseInt($(response).find('user > space_amount').text());
					var usedSpace = parseInt($(response).find('user > space_used').text());
					var user = {
						name : $(response).find('user > login').text(),
						total : ((totalSpace / 1024) / 1024) / 1024,
						used : ((usedSpace / 1024) / 1024) / 1024,
						per : (usedSpace / totalSpace) * 100,
					};
					boxSuccess(user);
				} else {
					BEAM.error('Unable to get box account info');					boxError();				}			},
			error : boxError
		});
	});
};
