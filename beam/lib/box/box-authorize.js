/**
 * @author Akash Chauhan <akash6190 [at] gmail [dot] com
 *
 */

function isBoxAuthorized() {
	// debugger;

	if ($('div.api_auth_top').text().match(/Thanks\ for\ logging/) != null) {
		//ask for ticket from the extension.
		var boxPort = chrome.extension.connect(chrome.i18n.getMessage("@@extension_id"), {
			name : 'boxPort'
		});
		boxPort.postMessage('get_auth_token');
		console.log('Message Sent');
	} else {
		console.log('Skip inquiry page.');
	}
	return;
}

isBoxAuthorized();
