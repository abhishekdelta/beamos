/*
 * Dropbox API Integration
 */

var BeamDropboxManager = function() {
};

BEAM.extend(BeamDropboxManager, BeamCloudInterface);

BeamDropboxManager.prototype.uploadFile = function(filename, contents, success, error) {
	var path = "/" + filename;
	var dropbox = new Dropbox(BeamConfig.DROPBOX_KEY, BeamConfig.DROPBOX_SECRET);
	dropbox.setDefaultError(error);

	var upload = function() {

		BEAM.log('File upload complete.');
		dropbox.putFileContents(path, contents, success, error);
	};
	dropbox.authorize(upload, error);
};

BeamDropboxManager.prototype.readFile = function(filename, success, error) {
	var path = "/" + filename;
	var dropbox = new Dropbox(BeamConfig.DROPBOX_KEY, BeamConfig.DROPBOX_SECRET);
	dropbox.setDefaultError(error);

	var contents = function() {
		dropbox.getFileContents(path, success, error);
	};
	dropbox.authorize(contents, error);
};

BeamDropboxManager.prototype.downloadFileLink = function(filename, success, error) {
	var path = "/" + filename;
	var dropbox = new Dropbox(BeamConfig.DROPBOX_KEY, BeamConfig.DROPBOX_SECRET);
	dropbox.setDefaultError(error);

	var getLink = function() {
		dropbox.getDirectLink(path, success, error);
	};
	dropbox.authorize(getLink, error);
};

BeamDropboxManager.prototype.getSpaceRemaining = function(success, error) {
	var dropbox = new Dropbox(BeamConfig.DROPBOX_KEY, BeamConfig.DROPBOX_SECRET);
	dropbox.setDefaultError(error);
	var getSize = function() {
		var sizeSuc = function(quota) {
			var remaining = (quota.quota_info.quota - quota.quota_info.normal - quota.quota_info.shared);
			success(remaining);
		};
		dropbox.getAccountInfo(sizeSuc, error);
	};
	dropbox.authorize(getSize, error);
};

BeamDropboxManager.prototype.getAccInfo = function(success, error) {
	var dropbox = new Dropbox(BeamConfig.DROPBOX_KEY, BeamConfig.DROPBOX_SECRET);
	dropbox.setDefaultError(error);
	var getSize = function() {
		var sizeSuc = function(quota) {
			var cloudObj = BeamStorageManager.getFileInfo('/', 'cloud');
			if (cloudObj) {
				cloudObj = JSON.parse(cloudObj);
				cloudObj['dropbox'] = {
					'id' : '/'
				};
			} else {
				cloudObj = {
					'dropbox' : {
						'id' : '/'
					}
				};
			}
			BeamStorageManager.setFileInfo('/', 'cloud', JSON.stringify(cloudObj));
			var user = {
				name : quota.display_name,
				total : ((quota.quota_info.quota / 1024) / 1024) / 1024,
				used : (((quota.quota_info.normal + quota.quota_info.shared) / 1024) / 1024) / 1024,
				per : ((quota.quota_info.normal + quota.quota_info.shared) / quota.quota_info.quota) * 100,
			};
			BeamCloudManager.dropbox.refresh();
			success(user);
		};
		dropbox.getAccountInfo(sizeSuc, error);
	};
	dropbox.authorize(getSize, error);
};

BeamDropboxManager.prototype.refresh = function(success, error) {
	if (!success) {
		success = function(res) {
			BEAM.log(res);
		};
	}
	if (!error) {
		error = function(res) {
			BEAM.error(res);
		};
	}
	var dropbox = new Dropbox(BeamConfig.DROPBOX_KEY, BeamConfig.DROPBOX_SECRET);
	dropbox.setDefaultError(error);
	var getDelta = function() {
		var deltaSuc = function(response) {
			//create the structure here
			//create entries
			var BFM = new BeamFileManager();
			BFM.initFS(function(dirPath) {
				var entries = response.entries;
				entries.forEach(function(value, key) {
					//
					// [<path>, <metadata>] - Indicates that there is a file/folder at the given path. You should add the entry to your local path. The metadata value is the same as what would be returned by the /metadata call, except folder metadata doesn't have hash or contents fields. To correctly process delta entries:
					// If the new entry includes parent folders that don't yet exist in your local state, create those parent folders in your local state.
					// If the new entry is a file, replace whatever your local state has at path with the new entry.
					// If the new entry is a folder, check what your local state has at <path>. If it's a file, replace it with the new entry. If it's a folder, apply the new <metadata> to the folder, but do not modify the folder's children.
					// [<path>, null] - Indicates that there is no file/folder at the given path. To update your local state to match, anything at path and all its children should be deleted. Deleting a folder in your Dropbox will sometimes send down a single deleted entry for that folder, and sometimes separate entries for the folder and all child paths. If your local state doesn't have anything at path, ignore this entry.

					if (!!value[1]) {
						// create entry
						path = value[1].path.split('/');
						var updateXML = function(path) {
							var cloudJson = null;
							var filesCollection = BeamStorageManager.getFilesCollection();
							var fileDoc = $('files', filesCollection);
							path = path.split('/');
							var lastFile = path.pop();
							var temp;
							var pathStr = '';
							path.forEach(function(v) {
								if (v != "") {
									pathStr += '/' + v;
									temp = $("file[name='" + v + "'][type='folder']", fileDoc);
									if (!!temp.attr('cloud')) {
										cloudJson = JSON.parse(temp.attr('cloud'));
										cloudJson['dropbox'] = {
											'id' : pathStr.toLowerCase()
										};
									} else {
										cloudJson = {
											'dropbox' : {
												'id' : pathStr.toLowerCase()
											}
										};
									}
									cloudJson = JSON.stringify(cloudJson);
									temp.attr('cloud', cloudJson);
									temp.attr('uploaded', true);
									fileDoc = temp;
								}
							});
							pathStr += '/' + lastFile;
							if (value[1].is_dir) {
								temp = $("file[name='" + lastFile + "'][type='folder']", fileDoc);
							} else {
								temp = $("file[name='" + lastFile + "'][type!='folder']", fileDoc);
							}
							if (!!temp.attr('cloud')) {
								cloudJson = JSON.parse(temp.attr('cloud'));
								cloudJson['dropbox'] = {
									'id' : value[0],
									'rev' : value[1].rev
								};
							} else {
								cloudJson = {
									'dropbox' : {
										'id' : value[0],
										'rev' : value[1].rev
									}
								};
							}
							cloudJson = JSON.stringify(cloudJson);
							temp.attr('cloud', cloudJson);
							temp.attr('uploaded', true);

							BeamStorageManager.setFilesCollection(filesCollection);
						}
						if (value[1].is_dir) {
							BFM.createDirectory(value[1].path, true, function(dirEntry) {
								updateXML(dirEntry.fullPath);
							}, 'dropbox');
						} else {
							BFM.createFile(value[1].path, true, function(fileEntry) {
								updateXML(fileEntry.fullPath)
							}, 'dropbox');
						}

					} else {
						// delete entry
						BEAM.log('Delete file or folder at ' + value[0]);
						// BFM.r;s					}
				});
			});
			BeamStorageManager.insertIntoCollection(BeamStorageCollections.DROPBOX_COLLECTION_KEY, "cursor", response.cursor);			if (response.has_more) {
				//if the current structure is different then only do the changes
				dropbox.getDeltaInfo(deltaSuc, error);
			}
			//save the cursor in DropBox_cofig
			success(response);
		};
		dropbox.getDeltaInfo(deltaSuc, error);
	};
	dropbox.authorize(getDelta, error);
}

BeamDropboxManager.prototype.open = function(fileName, cloudObject, saveFn, callback_) {

	//check if the file is download
	BeamCloudManager.dropbox.downloadFileLink(cloudObject.id, function(source) {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', source.url, true);
		xhr.onprogress = function() {
			BEAM.log("Downloading:" + source.url);
		};
		// xhr.overrideMimeType('text/plain; charset=x-user-defined');
		xhr.responseType = "blob";
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				BEAM.log("Completed");
				BEAM.log(xhr.getResponseHeader("Content-Type"));
				saveFn(xhr.response, xhr.getResponseHeader("Content-Type"), fileName, callback_);
			}
		};
		xhr.send(null);
	});}

BeamDropboxManager.prototype.getFileList = function(path, success, error) {
	path = "/" + path;
	var dropbox = new Dropbox(BeamConfig.DROPBOX_KEY, BeamConfig.DROPBOX_SECRET);
	dropbox.setDefaultError(error);

	var getList = function() {
		dropbox.getDirectoryContents(path, success, error);
	};
	dropbox.authorize(getList, error);
};
