var BeamBrowserActions = {

	processBrowserSaveAs : function(filename, folderId, isDirectory, content, fileUrl, embedded) {

		BEAM.log("Browser save as called");
		// First Step: Downloading the file to local storage
		// TODO:Ensure this id is not taken by next download
		var fileId = BeamFileManager.currentFileCount();
		
		// BEAM.log("fileid:"+fileId);		
		// var fileName = "BEAM_" + fileId;
		// var fileName = name;
		if (!content) {
			BEAM.log('content');
			BeamFileManager.downloadAndSave(fileUrl, fileName, onLocalSaveSuccess);
		} else if (embedded) {
			BEAM.log('embedded');
			BeamFileManager.save(fileUrl.toString(), "link", fileName, onLocalSaveSuccess);
		} else {
			BEAM.log('other');
			BeamFileManager.save(content, "text", fileName, onLocalSaveSuccess);
		}

		// Call back function to create an entry in the files collection once local save is done
		function onLocalSaveSuccess(mimetype, filesize, data) {
			BeamFileManager.createFileEntry(fileId, name, mimetype, isDirectory, folderId, filesize, fileUrl, function(fileEntry) {
				try {
						BeamCloudManager[fileEntry.cloudService.toLowerCase()]
						                 .uploadFile(name, data, function(cloudFid) {
											fileEntry.cloudFileId = cloudFid;
											BeamStorageManager.updateFilesCollection(fileId,fileEntry);
											BEAM.log("Uploaded to " + fileEntry.cloudService + " successfully, cloudFileId:" + cloudFid);
										 }, function(e) {
												BEAM.error("Error in uploading to " + fileEntry.cloudService + " " + e);											}
										 );				} catch(e) {
					BEAM.error("Error in uploading to " + fileEntry.cloudService + " " + e);
				}
			}, embedded);

			/*
			 if(fileEntry.cloudService === BeamConfig.DROPBOX_SERVICE_NAME){
			 BeamCloudManager.dropbox.uploadFile(fileName, data, function(){
			 BEAM.log("Uploaded to Dropbox successfully;");
			 }, function(e){
			 BEAM.error("Error in uploading to Dropbox "+e);
			 });
			 }
			 else if(fileEntry.cloudService === BeamConfig.BOXNET_SERVICE_NAME) {
			 BeamCloudManager.boxnet.uploadFile(fileName, fileId, data, function(){
			 BEAM.log("Uploaded to Boxnet successfully;");
			 }, function(e){
			 BEAM.error("Error in uploading to Boxnet "+e);
			 });
			 }
			 else if(fileEntry.cloudService === BeamConfig.SKYDRIVE_SERVICE_NAME){
			 BeamCloudManager.skydrive.uploadFile(fileName, data, function(){
			 BEAM.log("Uploaded to Skydrive successfully;");
			 }, function(e){
			 BEAM.error("Error in uploading to Skydrive "+e);
			 });
			 }
			 */
		};

	}
};
