var BEAM = {};

/* Bootstrapping Beam OS */
BEAM.init = function(settings) {
	initPersistentStorage();
	if (!settings) {		
		return BeamStorageManager.init();
	}		
}

BEAM.extend = function(Child, Parent) {
	var F = function() {
	};
	F.prototype = Parent.prototype;
	Child.prototype = new F();
	Child.prototype.constructor = Child;
	Child.uber = Parent.prototype;
};

// Info logging functionality
BEAM.log = function(message) {
	console.log(message);
};

// Error log functionality
BEAM.error = function(message) {
	console.log('ERROR:' + message);
};

// @TODO This displays the message to the user. Currently using browser's alert.
BEAM.alert = function(message, loglevel) {
	loglevel = loglevel || BeamConfig.LOG_LEVEL_INFO;
	webkitNotifications.createNotification('','',message).show();
};

