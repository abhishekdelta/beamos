/**
 * @author Akash Chauhan <akash [at] gmail [dot] com
 */

var BeamSugarSyncManager = function() {
};

BEAM.extend(BeamSugarSyncManager, BeamCloudInterface);

BeamSugarSyncManager.prototype.getAuthToken = function(callback) {
	//check if auth_token is present
	//if yes than return the token else generate one

	BEAM.log('Fetching Auth Token');
	var refresh_token = BeamStorageManager.findInCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, 'refresh_token');
	var expire_time = BeamStorageManager.findInCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, 'expire_time');
	// Check if auth_token is expired or not
	if (refresh_token == undefined) {
		var sugarSyncAuthWindowId;
		chrome.windows.create({
			url : 'https://tranquil-citadel-6176.herokuapp.com/redirect_uri/sugarsync',
			width : 480,
			height : 320,
			focused : true,
			type : "popup"
		}, function(window) {
			sugarSyncAuthWindowId = window.id;
		});

		BEAM.log('SUGARSYNC Auth Window created');
		chrome.extension.onConnect.addListener(function(port) {
			if (port.name == 'sugarSyncPort') {
				port.onMessage.addListener(function(msg) {
					//add refresh_token, access_token , expire_time
					BEAM.log('Saving access_token,refresh_token, expire_time');
					var access_token = BeamStorageManager.insertIntoCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, "access_token", msg.access_token);
					BeamStorageManager.insertIntoCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, "refresh_token", msg.refresh_token);
					BeamStorageManager.insertIntoCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, "expire_time", msg.expire_time);
					BEAM.log('Saving access_token,refresh_token, expire_time complete.');
					chrome.windows.remove(sugarSyncAuthWindowId);
					BEAM.log('SUGARSYNC Auth Window closed');
					callback(access_token);
				});
			}
		});

		BEAM.log('SUGARSYNC Auth Window connected');

	} else if (new Date(expire_time) < new Date()) {
		BEAM.log('SUGARSYNC access_token is expired, fetching new one.');
		$.ajax({
			url : 'https://tranquil-citadel-6176.herokuapp.com/redirect_uri/sugarsync?refresh=' + refresh_token,
			success : function(data) {
				BEAM.log("Updating SUGARSYNC tokens.");
				data = $(data);
				var access_token = BeamStorageManager.insertIntoCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, "access_token", data.find('access_token').text().trim());
				BeamStorageManager.insertIntoCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, "expire_time", data.find('expiration').text().trim());
				BEAM.log('Updating SUGARSYNC tokens complete.');
				callback(access_token);
			}
		});
	} else {
		// call back access_token
		callback(BeamStorageManager.findInCollection(BeamStorageCollections.SUGARSYNC_COLLECTION_KEY, 'access_token'))	}
};

/**
 *
 * @param String type  can be api/www
 * @param String resource
 * @param String method (optional)
 * @param String params (optional)
 * @param String api (optional) defaults to 2
 */
BeamBoxManager.prototype.generateURL = function(type, resource, method, params, api) {
	if (api == undefined) {
		api = '2.0/';
	} else {
		api += '.0/'
		params = $.extend({
			api_key : BeamConfig.BOX_KEY,
			auth_token : BeamCloudManager.box.getAuthToken()
		}, params);
	}
	resource = ((resource == undefined) ? '' : (resource + '/'));
	method = ((method == undefined) ? '' : method);
	params = ((params == undefined) ? '' : ('?' + $.param(params)));

	return 'https://' + type + '.box.com/' + api + resource + method + params;
};

/**
 * @param Object an xhr
 * @desc  To set BoxAuth Header
 */
BeamSugarSyncManager.prototype.headers = function(access_token) {
	return {
		'Authorization' : access_token ,
		'Content-Type': 'application/xml; charset=UTF-8'
	};
};

BeamSugarSyncManager.prototype.uploadFile = function(path, contents, sugarSyncSuccess, sugarSyncError) {
	BeamCloudManager.sugarsync.getAuthToken(function(access_token){
		$.ajax({
			url:'',
			headers : BeamCloudManager.sugarsync.headers(access_token),
			dataType : 'xml',
			success: function(response){
				
			}
		});
	});
	// folderId = '0';
	// if (folderId == undefined) {
		// boxError();
	// } else {
		// var fileLink = BeamCloudManager.box.generateURL('api', 'files', 'content');
		// var fData = new FormData();
		// fData.append('file', contents, path);
		// fData.append('folder_id', folderId);
// 
		// $.ajax({
			// url : fileLink,
			// data : fData,
			// type : 'POST',
			// contentType : false,
			// processData : false,
			// dataType : 'json',
			// headers : BeamCloudManager.box.headers(),
			// success : function(response) {
				// // console.log(response);
				// boxSuccess(response.id);
			// },
			// error : boxError
		// });
	// }
};

BeamSugarSyncManager.prototype.readFile = function(filename, success, error) {
	// GET /files/{file id}/content
};

BeamSugarSyncManager.prototype.downloadFileLink = function(filePath, success, error) {

};

BeamSugarSyncManager.prototype.getSpaceRemaining = function(boxSuccess, boxError) {
	//https://api.box.com/1.0/rest
	BEAM.log('Calculating Space Remaining');
	$.ajax({
		url : BeamCloudManager.box.generateURL('api', '', 'rest', {
			'action' : 'get_account_info',
			'api_key' : BeamConfig.BOX_KEY
		}, '1'),
		dataType : "xml",
		success : function(response) {
			if ($(response).find('status').text() == 'get_account_info_ok') {
				boxSuccess(parseInt($(response).find('user > space_amount').text()) - parseInt($(response).find('user > space_used').text()));
			} else {
				BEAM.error('Unable to get box account info');
				boxError();
			}
		},
		error : boxError
	});
};

BeamSugarSyncManager.prototype.getFileList = function(folderId, boxSuccess, boxError) {
	// https://api.box.com/2.0/folders/FOLDER_ID/items
	folderId = (folderId == undefined) ? '0' : folderId;
	$.ajax({
		url : BeamCloudManager.box.generateURL('api', 'folders', folderId + '/items'),
		headers : BeamCloudManager.box.headers(),
		dataType : "json",
		success : boxSuccess,
		error : boxError
	});
};

BeamSugarSyncManager.prototype.getAccInfo = function(boxSuccess, boxError) {
	//https://api.cx.com/1/users/self?access_token=${token}
	BEAM.log('Calculating Space Remaining');
	BeamCloudManager.box.getAuthToken(function() {
		BEAM.log("Auth token recieved");
		$.ajax({
			url : BeamCloudManager.box.generateURL('api', '', 'rest', {
				'action' : 'get_account_info',
				'api_key' : BeamConfig.BOX_KEY
			}, '1'),
			dataType : "xml",
			success : function(response) {
				if ($(response).find('status').text() == 'get_account_info_ok') {
					var totalSpace = parseInt($(response).find('user > space_amount').text());
					var usedSpace = parseInt($(response).find('user > space_used').text());
					var user = {
						name : $(response).find('user > login').text(),
						total : ((totalSpace / 1024) / 1024) / 1024,
						used : ((usedSpace / 1024) / 1024) / 1024,
						per : (usedSpace / totalSpace) * 100,
					};
					boxSuccess(user);
				} else {
					BEAM.error('Unable to get box account info');
					boxError();
				}
			},
			error : boxError
		});
	});
};
