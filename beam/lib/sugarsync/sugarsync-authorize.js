/**
 * @author Akash Chauhan <akash6190 [at] gmail [dot] com
 *
 */

function isSugarSyncAuthorized() {
	// debugger;
	if ($('body > h3 ').text().match(/Thanks\ for\ using\ SugarSync\!/) != null) {
		//ask for ticket from the extension.
		var ssTag = document.getElementsByTagName("sugarsync");
		var sugarSyncPort = chrome.extension.connect(chrome.i18n.getMessage("@@extension_id"), {
			name : 'sugarSyncPort'
		});

		sugarSyncPort.postMessage({
			'refresh_token' : ssTag[0].getElementsByTagName('refresh_token')[0].innerText.trim(),
			'access_token' : ssTag[0].getElementsByTagName('access_token')[0].innerText.trim(),
			'expire_time' : ssTag[0].getElementsByTagName('access_body')[0].getElementsByTagName('expiration')[0].innerText.trim()
		});
		console.log('Message Sent');
	} else {
		console.log('Skip inquiry page.');
	}
	return;
}

isSugarSyncAuthorized();
