/*
 * Dict of all the collections stored in the local storage.
 */
var BeamStorageCollections = {
	CLOUD_PRIORITY_KEY : "Beam_Cloud_Priority",
	FILES_COLLECTION_KEY : "Beam_Files",
	UI_COLLECTION_KEY : "Beam_UI",
	BOX_COLLECTION_KEY : "Beam_Box_Config",
	DROPBOX_COLLECTION_KEY : "Beam_Dropbox_Config",
	SKYDRIVE_COLLECTION_KEY : "Beam_Skydrive_Config",
	SUGARSYNC_COLLECTION_KEY : "Beam_SugarSync_Config",
	CX_COLLECTION_KEY : "Beam_Cx_Config"
};

/*
 * Local Storage Management
 */
var BeamStorageManager = (function() {

	// Currently using browser localStorage
	var storageInstance = localStorage;

	function initializeStorage() {

		BEAM.log("Initializing BeamStorageManager...");

		if (!storageInstance[BeamStorageCollections.FILES_COLLECTION_KEY]) {
			BEAM.log("Creating empty Files collection...");
			storageInstance[BeamStorageCollections.FILES_COLLECTION_KEY] = (new XMLSerializer()).serializeToString(document.implementation.createDocument(null, 'files', null));
		}

		if (storageInstance[BeamStorageCollections.UI_COLLECTION_KEY] == undefined) {
			BEAM.log("Creating empty UI collection...");
			storageInstance[BeamStorageCollections.UI_COLLECTION_KEY] = JSON.stringify({});
		}

		if (storageInstance[BeamStorageCollections.BOX_COLLECTION_KEY] == undefined) {
			BEAM.log("Creating empty Box collection...");
			storageInstance[BeamStorageCollections.BOX_COLLECTION_KEY] = JSON.stringify({});
		}

		if (storageInstance[BeamStorageCollections.DROPBOX_COLLECTION_KEY] == undefined) {
			BEAM.log("Creating empty Dropbox collection...");
			storageInstance[BeamStorageCollections.DROPBOX_COLLECTION_KEY] = JSON.stringify({});
		}

		if (storageInstance[BeamStorageCollections.SKYDRIVE_COLLECTION_KEY] == undefined) {
			BEAM.log("Creating empty Skydrive collection...");
			storageInstance[BeamStorageCollections.SKYDRIVE_COLLECTION_KEY] = JSON.stringify({});
		}
		if (storageInstance[BeamStorageCollections.SUGARSYNC_COLLECTION_KEY] == undefined) {
			BEAM.log("Creating empty Skydrive collection...");
			storageInstance[BeamStorageCollections.SUGARSYNC_COLLECTION_KEY] = JSON.stringify({});
		}

		if (storageInstance[BeamStorageCollections.CX_COLLECTION_KEY] == undefined) {
			BEAM.log("Creating empty CX collection...");
			storageInstance[BeamStorageCollections.CX_COLLECTION_KEY] = JSON.stringify({});
		}

		BEAM.log(storageInstance[BeamStorageCollections.CLOUD_PRIORITY_KEY]);

		// Creating an empty priority key which will be updated everytime priority is changed
		if (storageInstance[BeamStorageCollections.CLOUD_PRIORITY_KEY] == undefined) {
			BEAM.log('Cloud priority key not found, creating empty key & redirecting to Settings page...');
			storageInstance[BeamStorageCollections.CLOUD_PRIORITY_KEY] = JSON.stringify({});
			return false;
		} else if ($.isEmptyObject(this.getCollection(BeamStorageCollections.CLOUD_PRIORITY_KEY))) {
			BEAM.log('Cloud priority key not found, redirecting to Settings page...');
			return false;
		} else {
			BEAM.log('BeamStorageManager initialized');
			return true;
		}
	}

	return {
		init : initializeStorage,

		getUsedSpace : function() {

		},
		getTotalSpace : function() {

		},

		getCollection : function(collectionId, isXML) {
			BEAM.log("Get collection:" + collectionId);
			try {
				var data = null;
				if (!isXML) {
					data = JSON.parse(storageInstance[collectionId]);
				} else {
					// data = (new DOMParser()).parseFromString(storageInstance[collectionId],'text/xml');
					data = $.parseXML(storageInstance[collectionId]);				}
				// BEAM.log(data);
				return data;
			} catch(e) {
				BEAM.error("Could not find in local storage the key:" + collectionId);
			}
		},
		setCollection : function(collectionId, collectionData, isXML) {
			try {
				if (!isXML) {
					collectionData = JSON.stringify(collectionData);
				} else {
					collectionData = (new XMLSerializer()).serializeToString(collectionData);
				}
				// BEAM.log("Set collection:" + collectionId + " -> " + collectionData);
				storageInstance[collectionId] = collectionData;
			} catch(e) {				BEAM.error("Could not save to local storage:" + collectionId);
			}
		},

		// Files Collection Accessors
		
		/**
		 *
		 * @param String path
		 * path value is passed as string eg '/a/b/c'
		 * is converted into files > file[name='a'] > file[name=b] > file[name='c']
		 */
		generateXMLQuery : function(path) {
			var x = "files > ";
			path.split('/').forEach(function(v) {
				if (v != "") {
					x += "file[name='" + v + "'] > ";
				}
			});
			return x.slice(0, -2);
		},
		getFilesCollection : function() {
			return this.getCollection(BeamStorageCollections.FILES_COLLECTION_KEY, 1);
		},
		setFilesCollection : function(obj) {
			this.setCollection(BeamStorageCollections.FILES_COLLECTION_KEY, obj, 1);
		},
		setFileInfo : function(filePath, attribute, value) {
			var data = this.getFilesCollection();
			$(this.generateXMLQuery(filePath),data).attr(attribute,value);
			this.setFilesCollection(data);
		},
		getFileInfo : function(filePath, attribute) {
			return $(this.generateXMLQuery(filePath),this.getFilesCollection()).attr(attribute);s
		},

		// UI Collection Accessors
		getUICollection : function() {
			return this.getCollection(BeamStorageCollections.UI_COLLECTION_KEY);
		},
		setUICollection : function(obj) {
			this.setCollection(BeamStorageCollections.UI_COLLECTION_KEY, obj);
		},
		updateUICollection : function(key, value) {
			var data = this.getUICollection();
			data[key] = value;
			this.setUICollection(data);
		},

		// Generic Collection Actions
		insertIntoCollection : function(collectionKey, key, value) {
			var data = this.getCollection(collectionKey);
			data[key] = value;
			storageInstance[collectionKey] = JSON.stringify(data);
			return value;
		},
		findInCollection : function(collectionKey, key) {
			var data = this.getCollection(collectionKey);
			var value = data[key];
			return value;
		}
	};
})();

/*
 * Persistent Storage Management.
 */
var BeamPersistentStorage;

function initPersistentStorage() {
	window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
	window.requestFileSystem(window.PERSISTENT, 30 * 1024 * 1024, function(fs) {
		BEAM.log('Initializing BeamPersistentStorage. Opened file system: ' + fs.name);
		BeamPersistentStorage = fs;
	}, function(e) {
		var msg = '';

		switch (e.code) {
			case FileError.QUOTA_EXCEEDED_ERR:
				msg = 'QUOTA_EXCEEDED_ERR';
				break;
			case FileError.NOT_FOUND_ERR:
				msg = 'NOT_FOUND_ERR';
				break;
			case FileError.SECURITY_ERR:
				msg = 'SECURITY_ERR';
				break;
			case FileError.INVALID_MODIFICATION_ERR:
				msg = 'INVALID_MODIFICATION_ERR';
				break;
			case FileError.INVALID_STATE_ERR:
				msg = 'INVALID_STATE_ERR';
				break;
			default:
				msg = 'Unknown Error';
				break;
		};
		BEAM.error(msg);
	});
}

