/**
 * Single access for all the cloud implementations.
 **/
var BeamCloudManager = (function() {
	return {
		dropbox   	: new BeamDropboxManager(),
		cx        	: new BeamCxManager(),
		skydrive  	: new BeamSkydriveManager(),
		box    		: new BeamBoxManager(),
		sugarsync 	: new BeamSugarSyncManager(),
	}
})();

