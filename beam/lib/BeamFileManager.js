/*
 * var BeamFile = function(filename, filetype, isDirectory, folderId, fileSize, cloudService, fileUrl, embedded, cloudFileId) {
 BEAM.log("Creating a new file entry:" + filename);
 this.name = filename;
 this.type = filetype;
 this.isDirectory = isDirectory;
 this.folderId = folderId;
 this.fileSize = fileSize;
 this.cloudService = cloudService;
 this.fileUrl = fileUrl;
 this.cloudFileId = cloudFileId;
 this.embedded = embedded;
 };

 var BeamFileManager = (function(){
 //requestQuota

 var currentFolderPath;
 var services;
 var fileId;
 function fileList(path){
 //save everything in a multi dimensional json array.
 this.getpathId();
 };
 function fileInfo(){};
 return {

 };
 })();
 var BeamFileManager = {

 currentFileCount : function() {
 return BeamStorageManager.getFilesCollection().length;
 },

 getFileName : function(fileid) {
 BeamStorageManager.getFilesCollection()[fileid]["name"];
 },
 getFileType : function(fileid) {
 BeamStorageManager.getFilesCollection()[fileid]["type"];
 },

 createFileEntry : function(fileid, name, type, isDirectory, folderId, fileSize, fileUrl, callback, embedded, cloudFileId) {
 BeamCloudStorageAllocator.getAvailableCloudService(fileSize, function(cloudService) {

 BEAM.log("Creating a new file entry " + name + " with id " + fileid + " into localstorage...");
 cloudFileId = cloudFileId || null;

 var newFileEntry = new BeamFile(name, type, isDirectory, folderId, fileSize, cloudService, fileUrl, embedded, cloudFileId);
 BEAM.log(newFileEntry);
 BeamStorageManager.updateFilesCollection(fileid, newFileEntry);

 if (callback != undefined) {
 callback(newFileEntry);
 }
 });
 },

 readDirectory : function(id) {
 BEAM.log("Reading directory with id:" + id);

 id = parseInt(id, 10);
 var files = BeamStorageManager.getFilesCollection();
 var containingFiles = new Array();
 if (files[id].isDirectory) {
 for (key in files) {
 if (files[key] != null) {
 if(files[key].folderId === id) {
 containingFiles.push({beamId: key, file: files[key]});
 }
 }
 else {
 BEAM.error("A null entry in Files collection is found with key:" + key);
 }
 }
 BEAM.log(containingFiles);
 return containingFiles;
 } else {
 BEAM.log("Not a directory");
 }
 }
 };
 */

var util = util || {};
util.toArray = function(list) {
	return Array.prototype.slice.call(list || [], 0);
};

var BeamFileManager = function(fsn_) {
	window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;

	var fs_ = null;
	var cwd_ = null;
	var history_ = [];
	var histpos_ = 0;
	var histtemp_ = 0;

	var timer_ = null;

	/**
	 *Functions related to editing XML Starts here
	 */

	/**
	 * @param String path  eg: /a/b/c
	 * @param Boolean isDir  true if the last element is diretory (default: true)
	 * @param Object attObj
	 *
	 */
	function createXMLPath_(path, isDir, cloudObject) {
		//this function will create path only in xml file after this function is successfully executed
		//start upload process
		isDir = (isDir == undefined) ? true : isDir;

		var cloudObjStr = null;
		if (cloudObject) {
			cloudObjStr = {};
			cloudObjStr[cloudObject] = {};
			cloudObjStr = JSON.stringify(cloudObjStr);
		}

		var filesCollection = BeamStorageManager.getFilesCollection();
		var fileDoc = $('files', filesCollection);
		var cloudJson = fileDoc.attr('cloud');
		var lastFile = null;
		path = path.split('/');
		if (!isDir) {
			lastFile = path.pop();
		}
		var temp;
		path.forEach(function(v) {
			if (v != "") {
				temp = $("file[name='" + v + "'][type='folder']", fileDoc);
				if (!temp.length) {
					fileDoc.append($('<file name="' + v + '" type="folder" uploaded="false" ></file>'));
					temp = $("file[name='" + v + "'][type='folder']", fileDoc);
					if (cloudObjStr) {
						temp.attr('cloud', cloudObjStr);
					}
				} else if (!!temp.attr('cloud') || cloudObjStr) {
					cloudJson = temp.attr('cloud');
					if (cloudJson) {
						if (cloudObjStr) {
							cloudJson = JSON.parse(cloudJson);
							cloudJson[cloudObject] = {};
							cloudJson = JSON.stringify(cloudJson);
							temp.attr('cloud', cloudJson);
						}
					} else if (cloudObjStr) {
						cloudJson = cloudObjStr;
						temp.attr('cloud', cloudJson);
					}

				}
				fileDoc = temp;
			}
		});
		if (!isDir) {
			temp = $("file[name='" + lastFile + "'][type!='folder']", fileDoc);
			if (!temp.length) {
				fileDoc.append($('<file name="' + lastFile + '" type="file" downloaded="false" uploaded="false" ></file>'));
				temp = $("file[name='" + lastFile + "'][type!='folder']", fileDoc);
				if (cloudObjStr) {
					temp.attr('cloud', cloudObjStr);
				}
			} else if (!!temp.attr('cloud') || cloudObjStr) {
				cloudJson = temp.attr('cloud');
				if (cloudJson) {
					if (cloudObjStr) {
						cloudJson = JSON.parse(cloudJson);
						cloudJson[cloudObject] = {};
						cloudJson = JSON.stringify(cloudJson);
						temp.attr('cloud', cloudJson);
					}
				}
			}
		}

		BeamStorageManager.setFilesCollection(filesCollection);
		return $.parseJSON(cloudJson);
	}

	/**
	 *Functions related to editing XML ends here
	 */

	function invalidOpForEntryType_(e, cmd, dest) {
		//Tell fsn that there is an error
		if (fsn_) {
			fsn_.contentWindow.postMessage({
				cmd : 'error',
				data : {
					error : e,
					cmd : cmd
				}
			}, location.origin);
		}
		if (e.code == FileError.NOT_FOUND_ERR) {
			BEAM.alert(cmd + ': ' + dest + ': No such file or directory');
		} else if (e.code == FileError.INVALID_STATE_ERR) {
			BEAM.alert(cmd + ': ' + dest + ': Not a directory');
		} else if (e.code == FileError.INVALID_MODIFICATION_ERR) {
			BEAM.alert(cmd + ': ' + dest + ': File or folder already exists');
		} else {
			errorHandler_(e);
		}
	}

	function errorHandler_(e) {
		var msg = '';
		switch (e.code) {
			case FileError.QUOTA_EXCEEDED_ERR:
				msg = 'QUOTA_EXCEEDED_ERR';
				break;
			case FileError.NOT_FOUND_ERR:
				msg = 'NOT_FOUND_ERR';
				break;
			case FileError.SECURITY_ERR:

				msg = 'SECURITY_ERR';
				break;
			case FileError.INVALID_MODIFICATION_ERR:
				msg = 'INVALID_MODIFICATION_ERR';
				break;
			case FileError.INVALID_STATE_ERR:
				msg = 'INVALID_STATE_ERR';
				break;
			default:
				msg = 'Unknown Error';
				break;
		};
		BEAM.error('Error: ' + msg);
	}

	function cd_(dest, callback_) {
		cwd_.getDirectory(dest, {}, function(dirEntry) {
			cwd_ = dirEntry;
			BEAM.log(cwd_.fullPath);

			// Tell FSN visualizer that we're cd'ing.
			if (fsn_) {
				fsn_.contentWindow.postMessage({
					cmd : 'cd',
					data : dest
				}, location.origin);

			}
			if (callback_)
				callback_();
			// console.log(fsn_);

		}, function(e) {
			invalidOpForEntryType_(e, 'cd', dest);
		});
	}

	function createDir_(rootDirEntry, folders, callback_, cloudOject, opt_errorCallback) {
		var errorCallback = opt_errorCallback || errorHandler_;

		rootDirEntry.getDirectory(folders[0], {
			create : true
		}, function(dirEntry) {
			// Recursively add the new subfolder if we still have a subfolder to create, else add this entry in Beam_Fils
			if (folders.length) {
				createDir_(dirEntry, folders.slice(1), callback_, cloudOject);
			} else {
				createXMLPath_(dirEntry.fullPath, true, cloudOject);
			}
			if (callback_) {
				callback_(dirEntry);
			}
		}, errorCallback);
	}

	/***
	 *
	 * @param String path
	 * @param JSONObject cloudJSON
	 * @TODO get cloudJSON if not defined
	 * Upload files in the specified service.
	 *
	 */
	function upload_(fileData, path, cloudJSON, callback_) {
		//get cloud service where we need to upload the file
		if (cloudJSON != undefined) {
			BeamCloudStorageAllocator.getAvailableCloudService(fileData.size, cloudJSON, function(service) {
				//createstucture
				BeamCloudManager[service].uploadFile(path, fileData, callback_, function(e) {
					BEAM.alert('Unable to Upload file');
				});
				//uploadfile
				//callback
			});
		}
		//create folder structure in the required cloud service and update cloud info of the folder
		//upload the file in required service and update the file uploaded
	}

	function save_(data, mimetype, filename, callback) {
		cwd_.getFile(filename, {
			create : true
		}, function(fileEntry) {
			//Create Entry inside Beam_Files
			var cloudJSON = createXMLPath_(fileEntry.fullPath, false);
			
			// FileWriter
			BEAM.log('type::');
			if ($.type(data) != 'object') {
				data = new Blob([data], {
					type : 'text/html'
				});
			}

			fileEntry.createWriter(function(writer) {
				writer.onprogress = function() {
					BEAM.log("Writing to file at location ", fileEntry.fullPath);
				};
				writer.onwriteend = function(e) {
					BEAM.log("Write completed.");
					//update all the file on cloud
					BeamStorageManager.setFileInfo(fileEntry.fullPath, 'type', mimetype)
					BeamStorageManager.setFileInfo(fileEntry.fullPath, 'downloaded', true);
					BEAM.log(cloudJSON);
					if (BeamStorageManager.getFileInfo(fileEntry.fullPath, 'uploaded').toLowerCase().trim() !== 'true') {
						upload_(data, fileEntry.fullPath, cloudJSON, function() {
							callback(fileEntry.toURL());
						});
					} else {
						callback(fileEntry.toURL());
					}
					//after upload successfull , update the relevant entry in Beam_Files

				};
				writer.onerror = function(e) {
				};
				writer.write(data);
			}, errorHandler_);
		});
	}

	/***
	 *
	 * @param String fileName
	 * @param Boolean createStructure
	 * @TODO handle when createStructure is false
	 * Create file with structure
	 *
	 */
	function touch_(fileName, createStructure, callback_, cloudObject) {
		// console.log(fileName);

		var createFile_ = function(name) {
			cwd_.getFile(name, {
				create : true
			}, function(fileEntry) {
				createXMLPath_(fileEntry.fullPath, false, cloudObject);
				// Tell FSN visualizer that we're touch'ing.
				if (fsn_) {
					fsn_.contentWindow.postMessage({
						cmd : 'touch',
						data : fileName
					}, location.origin);
				}
				if (callback_) {
					callback_(fileEntry);
				}
			}, function(e) {
				invalidOpForEntryType_(e, 'touch', fileName);
			});
		};
		if (createStructure) {
			var dirPath = fileName.split('/');
			fileName = dirPath.pop();
			dirPath = dirPath.join('/');
			BEAM.log('Create Structure for file');
			if (!!dirPath) {
				mkdir_(dirPath, true, function(path) {
					createFile_(path.fullPath + '/' + fileName);
				}, cloudObject);
			} else {
				createFile_('/' + fileName);
			}
		} else {
			createFile_(fileName);
		}

	}

	function copy_(src, dest, del) {

		if (!src || !dest) {
			BEAM.error('source or target not mentioned');
			return;
		}

		var runAction = function(cmd, srcDirEntry, destDirEntry, opt_newName) {
			var newName = opt_newName || null;
			if (cmd) {
				srcDirEntry.moveTo(destDirEntry, newName);
			} else {
				srcDirEntry.copyTo(destDirEntry, newName);
			}
		};

		// Moving to a folder? (e.g. second arg ends in '/').
		if (dest[dest.length - 1] == '/') {
			cwd_.getDirectory(src, {}, function(srcDirEntry) {
				// Create blacklist for dirs we can't re-create.
				var create = ['.', './', '..', '../', '/'].indexOf(dest) != -1 ? false : true;

				cwd_.getDirectory(dest, {
					create : create
				}, function(destDirEntry) {
					runAction(!!del, srcDirEntry, destDirEntry);
				}, errorHandler_);
			}, function(e) {
				// Try the src entry as a file instead.
				cwd_.getFile(src, {}, function(srcDirEntry) {
					cwd_.getDirectory(dest, {}, function(destDirEntry) {
						runAction(!!del, srcDirEntry, destDirEntry);
					}, errorHandler_);
				}, errorHandler_);
			});
		} else {// Treat src/destination as files.
			cwd_.getFile(src, {}, function(srcFileEntry) {
				srcFileEntry.getParent(function(parentDirEntry) {
					runAction(!!del, srcFileEntry, parentDirEntry, dest);
				}, errorHandler_);
			}, errorHandler_);
		}
	}

	function read_(path, successCallback) {
		if (!fs_) {
			return;
		}

		cwd_.getFile(path, {}, function(fileEntry) {
			fileEntry.file(function(file) {
				var reader = new FileReader();

				reader.onloadend = function(e) {
					successCallback(this.result);
				};

				reader.read(file);
			}, errorHandler_);
		}, function(e) {
			if (e.code == FileError.INVALID_STATE_ERR) {
				BEAM.error(path + ': is a directory');
			} else if (e.code == FileError.NOT_FOUND_ERR) {
				BEAM.error(path + ': No such file or directory');
			}
		});
	}

	function ls_(successCallback) {
		if (!fs_) {
			return;
		}

		//get current directories cloud info
		//fetch for cloud info
		// Read contents of current working directory. According to spec, need to
		// keep calling readEntries() until length of result array is 0. We're
		// guarenteed the same entry won't be returned again.
		var entries = [];
		var reader = cwd_.createReader();

		var readEntries = function() {
			reader.readEntries(function(results) {
				if (!results.length) {
					entries = entries.sort();
					successCallback(entries);
				} else {
					entries = entries.concat(util.toArray(results));
					readEntries();
				}
			}, errorHandler_);
		};

		readEntries();
	}

	function mkdir_(dirName, dashP, callback_, cloudObject) {
		if (!!dashP) {
			var folders = dirName.split('/');

			// Throw out './' or '/' if present on the beginning of our path.
			if (folders[0] == '.' || folders[0] == '') {
				folders = folders.slice(1);
			}

			createDir_(cwd_, folders, callback_, cloudObject);
		} else {
			cwd_.getDirectory(dirName, {
				create : true
			}, function(dirEntry) {

				createXMLPath_(dirEntry.fullPath, true, cloudObject);
				// Tell FSN visualizer that we're mkdir'ing.
				if (fsn_) {
					fsn_.contentWindow.postMessage({
						cmd : 'mkdir',
						data : dirName
					}, location.origin);
				}
				if (callback_) {
					callback_(dirEntry);
				}
			}, function(e) {
				invalidOpForEntryType_(e, 'mkdir', dirName);
			});
		}
	}

	function rmdir_(dirName) {
		cwd_.getDirectory(dirName, {}, function(dirEntry) {
			dirEntry.remove(function() {
				// Tell FSN visualizer that we're rmdir'ing.
				if (fsn_) {
					fsn_.contentWindow.postMessage({
						cmd : 'rm',
						data : dirName
					}, location.origin);
				}
			}, function(e) {
				if (e.code == FileError.INVALID_MODIFICATION_ERR) {
					BEAM.error(dirName + ': Directory not empty<br>');
				} else {
					errorHandler_(e);
				}
			});
		}, function(e) {
			invalidOpForEntryType_(e, 'rmdir', dirName);
		});

	}

	function rm_(fileName, recursive) {

		// Remove each file passed as an argument.
		recursive = !!recursive;
		cwd_.getFile(fileName, {}, function(fileEntry) {
			fileEntry.remove(function() {
				// Tell FSN visualizer that we're rm'ing.
				if (fsn_) {
					fsn_.contentWindow.postMessage({
						cmd : 'rm',
						data : fileName
					}, location.origin);
				}
			}, errorHandler_);
		}, function(e) {
			if (recursive && e.code == FileError.TYPE_MISMATCH_ERR) {
				cwd_.getDirectory(fileName, {}, function(dirEntry) {
					dirEntry.removeRecursively(null, errorHandler_);
				}, errorHandler_);
			} else if (e.code == FileError.INVALID_STATE_ERR) {
				BEAM.error(fileName + ': is a directory<br>');
			} else {
				errorHandler_(e);
			}
		});
	}

	return {
		initFS : function(callback_, size) {
			if (size == undefined) {
				size = 1024 * 1024;
			}
			if (!!!window.requestFileSystem) {
				BEAM.alert('Sorry! The FileSystem APIs are not available in your browser.');
				return;
			}
			window.requestFileSystem(window.PERSISTENT, size, function(filesystem) {
				fs_ = filesystem;
				cwd_ = fs_.root;
				type_ = window.PERSISTENT;
				size_ = size;

				// If we get this far, attempt to create a folder to test if the
				// --unlimited-quota-for-files fag is set.
				cwd_.getDirectory('testquotaforfsfolder', {
					create : true
				}, function(dirEntry) {
					dirEntry.remove(function() {// If successfully created, just delete it.
						if (callback_)
							callback_();
					});
				}, function(e) {
					if (e.code == FileError.QUOTA_EXCEEDED_ERR) {
						BEAM.error('ERROR: Write access to the FileSystem is unavailable.<br>');
						BEAM.alert('Chrome with the --unlimited-quota-for-files flag.');
					} else {
						errorHandler_(e);
					}
				});

			}, errorHandler_);
		},
		save : save_,
		downloadAndSave : function(source, filename, callback_) {
			var xhr = new XMLHttpRequest();
			xhr.open('GET', source, true);
			xhr.onprogress = function() {
				BEAM.log("Downloading:" + source);
			};
			// xhr.overrideMimeType('text/plain; charset=x-user-defined');
			xhr.responseType = "blob";
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					BEAM.log("Completed");
					BEAM.log(xhr.getResponseHeader("Content-Type"));
					save_(xhr.response, xhr.getResponseHeader("Content-Type"), filename, callback_);
				}
			};
			xhr.send(null);
		},
		list : ls_,
		readFile : function(filename, success) {
			if (!fileName) {
				BEAM.error('usage: ' + cmd + ' filename');
				return;
			}
			read_(fileName, success);
		},
		remove : rm_,
		createFile : touch_,
		changeDirectory : cd_,
		currentDirectory : function() {
			return cwd_.fullPath;
		},
		createDirectory : mkdir_,
		removeDirectory : rmdir_,
		open : function(fileName, success) {
			BEAM.log('Opening File');
			if (!fileName) {
				BEAM.error(' filename undefined');
				return;
			}
			cwd_.getFile(fileName, {}, function(fileEntry) {
				BEAM.log('Verifying the file is Downloaded' + fileEntry.fullPath);
				console.log(fileEntry);
				if (BeamStorageManager.getFileInfo(fileEntry.fullPath, 'downloaded').toLowerCase().trim() === 'true') {
					BEAM.log('Opening the downloaded file ' + fileEntry.fullPath);
					if (!success) {
						var myWin = window.open(fileEntry.toURL(), 'mywin');
					} else {
						success(fileEntry.toURL());
					}
				} else {
					BEAM.log('Downloading file ' + fileEntry.fullPath);
					var cloudObject = BeamStorageManager.getFileInfo(fileEntry.fullPath, 'cloud');
					if (cloudObject) {
						cloudObject = JSON.parse(cloudObject);
						var cloudSer = null;
						for (key in cloudObject) {
							BeamCloudManager[key].open(fileName, cloudObject[key], save_, success);
							break;
						};
					}
				}
			}, function(e) {
				BEAM.alert('Unable to open file ' + fileName);
			});
			// open_(fileName, success);
		},
		copy : function(src, dest) {
			return copy_(src, dest, false);
		},
		move : function(src, dest) {
			return copy_(src, dest, true);
		},
		addDroppedFiles : function(files) {
			util.toArray(files).forEach(function(file, i) {
				cwd_.getFile(file.name, {
					create : true,
					exclusive : true
				}, function(fileEntry) {

					// Tell FSN visualizer we've added a file.
					if (fsn_) {
						fsn_.contentWindow.postMessage({
							cmd : 'touch',
							data : file.name
						}, location.origin);
					}

					fileEntry.createWriter(function(fileWriter) {
						fileWriter.write(file);
					}, errorHandler_);
				}, errorHandler_);
			});
		},
		currentFileCount : function() {
			return BeamStorageManager.getFilesCollection().length;
		},
	}
};
