var BeamCloudStorageAllocator = {
	getService : function(priorityArr, contentSize, success) {
		BEAM.log("Trying to find a service with min size:" + contentSize);

		var service = priorityArr.pop();
		BEAM.log('Current service priorities: ' + service);

		if (service == undefined) {
			BEAM.error('No services found.');
			return;
		}

		BeamCloudManager[service].getSpaceRemaining(function(availableSpace) {
			if (contentSize <= availableSpace) {
				avlService = BeamConfig[service.toUpperCase() + '_SERVICE_NAME'];
				BEAM.log('Available space found: ' + availableSpace + '. Using:' + avlService);
				success(avlService.toLowerCase());
			} else {
				BeamCloudStorageAllocator.getService(priorityArr, contentSize, success);
			}
		}, function() {
			BeamCloudStorageAllocator.getService(priorityArr, contentSize, success);
		});
	},

	/***
	 *
	 * @param Integer contentsize
	 * @param String path
	 * @param function success
	 * get available service
	 */
	getAvailableCloudService : function(contentsize, folderCloud, success) {

		//get the cloud json object
		//combine and sort cloud priority
		/**
		 * sorting algorithm -> considering no of cloud services less than 1000,
		 * multiply the order in cloudPriority by 1000 then divide all the entries that are in folder cloudObject by 1000
		 * sort descending according to all the cloud priority.
		 * pass to getService;
		 */
		var cloudPriorObj = new Object();
		var cloudPriority = BeamStorageManager.getCollection(BeamStorageCollections.CLOUD_PRIORITY_KEY);
		$.each(cloudPriority, function(priority, service) {
			delete cloudPriorObj[service];
			if (folderCloud.hasOwnProperty(service)) {
				cloudPriorObj[(parseInt(priority) + 1)] = service;
			} else {
				cloudPriorObj[(parseInt(priority) + 1) * 1000] = service;
			}

		});

		var cloudPriorArr = new Array();
		$.each(cloudPriorObj, function(service) {
			cloudPriorArr.push(parseInt(service));
		});
		cloudPriorArr.sort(function(a, b) {
			return b - a;
		});
		cloudPriorArr.forEach(function(value, index) {
			cloudPriorArr[index] = cloudPriorObj[value];
		});
		delete cloudPriorObj;

		try {
			BeamCloudStorageAllocator.getService(cloudPriorArr, contentsize, success);
		} catch(e) {
			BEAM.error("Error allocating space " + contentsize + " in cloud: " + e.toString());
		}
	}
};
