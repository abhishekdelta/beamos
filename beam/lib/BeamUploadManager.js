var BeamUploadManager = {
		
	upload: function(blobdata, url, filename, callback) {
		var xhr = new XMLHttpRequest();
	    xhr.open('POST',url, true);
	    xhr.onload = function(e) {
	  		 //BEAM.log(e);
	    };
	    xhr.onreadystatechange = function () { 
   			 if (xhr.readyState == 4) { 
	  			callback(xhr.response); 	 
	   	 	 } 
  	    }; 
  	    // Listen to the upload progress.
  	    /*
		var progressBar = document.querySelector('progress');
		xhr.upload.onprogress = function(e) {
		    if (e.lengthComputable) {
		      progressBar.value = (e.loaded / e.total) * 100;
		      progressBar.textContent = progressBar.value; // Fallback for unsupported browsers.
		    }
		};
		*/  	   
		//Currently, the file content is sent in the "file" field as required for box integration.
		var formData = new FormData();
  		formData.append('file', blobdata, filename);
	    xhr.send(formData); 
	    BEAM.log("Upload request sent!");
	},
	
	// Use this method for uploading any content to a given url. 
	// On success the callback is called with the response data.
	uploadFile: function(contents, url, filename, callback) {
		BlobBuilder = window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder;
		var bb = new BlobBuilder();
		bb.append(contents);
		this.upload(bb.getBlob('text/plain'),url,filename,callback);
	}
};