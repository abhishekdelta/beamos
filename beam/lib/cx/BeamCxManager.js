/**
 * @author Akash Chauhan <akash [at] gmail [dot] com
 */

var BeamCxManager = function() {
};

BEAM.extend(BeamCxManager, BeamCloudInterface);

BeamCxManager.prototype.getAuthToken = function(callback) {
	//check if auth_token is present
	//if yes than return the token else generate one

	BEAM.log('Fetching Auth Token');
	if (BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token') == undefined) {
		var cxAuthWindowId;
		chrome.windows.create({
			url : 'https://www.cx.com/mycx/oauth/authorize?client_id=' + BeamConfig.CX_KEY + '&redirect_uri=https://tranquil-citadel-6176.herokuapp.com/redirect_uri/cx',
			width : 640,
			height : 480,
			focused : true,
			type : "popup"
		}, function(window) {
			cxAuthWindowId = window.id;
		});
		chrome.extension.onConnect.addListener(function(port) {
			if (port.name == 'cxPort') {
				port.onMessage.addListener(function(msg) {
					if (msg != null) {
						return BeamStorageManager.insertIntoCollection(BeamStorageCollections.CX_COLLECTION_KEY, "access_token", msg.access_token);
						BEAM.log('CX Access_Token Stored');
					} else {
						BEAM.error('User denied access to CX account');
					}
					chrome.windows.remove(cxAuthWindowId);
					BEAM.log('CX Auth Window closed');
					if (callback != undefined)
						callback(BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token'));
					return BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token');
				});
			}
		});
	} else {
		if (callback != undefined) {
			callback(BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token'));
		}
		return BeamStorageManager.findInCollection(BeamStorageCollections.CX_COLLECTION_KEY, 'access_token');
	};
};

/**
 *
 * @param String type  can be api/data
 * @param String resource
 * @param String method (optional)
 * @param String params (optional)
 * @param String api (optional) defaults to 1
 */
BeamCxManager.prototype.generateURL = function(type, resource, method, params, api) {
	params = $.extend({
		access_token : BeamCloudManager.cx.getAuthToken()
	}, params);
	api = ((api == undefined) ? '1/' : (api + '/'));
	resource = ((resource == undefined) ? '' : (resource + '/'));
	method = ((method == undefined) ? '' : method);
	params = ((params == undefined) ? '' : ('?' + $.param(params)));

	return 'https://' + type + '.cx.com/' + api + resource + method + params;
}

BeamCxManager.prototype.uploadFile = function(filePath, contents, cxSuccess, cxError) {
	/*
	* Request Type    POST
	* Resource        data
	* Path Param      path                This is File Path that specifies the upload destination
	* Querey Param    access_token
	*                 client_id           Your mashery Client_ID for verification
	*                 content_md5         Optional Url-Safe encoded Base64 String - Highly Recommended
	*                 content_sha1        Optional Url-Safe encoded Base64 String - Highly Recommended
	*                 file_size           Required for files over 10mb. Highly recommended otherwise.
	*                 file_content_type   MIME Type of the file being uploaded
	* Form Param      file                The file data (raw bytes).
	*
	* https://data.cx.com/1/data/self:/file1.jpg?accessToken=MySecretToken
	*/
	//https://data.cx.com/1/data/self:/asd.jpg?access_token=2nbjraqwa8w7abvn2e4bv5k8&client_id=q88rupr527bp7ywzfjysfuey
	if (filePath == undefined) {
		error();
	} else {
		var fileLink = BeamCloudManager.cx.generateURL('data', 'data', 'self:/' + encodeURIComponent(filePath) + '/', {
			client_id : BeamConfig.CX_KEY
		});

		var fData = new FormData();
		fData.append('file', contents);
		$.ajax({
			url : fileLink,
			data : fData,
			type : 'POST',
			contentType : false,
			processData : false,
			dataType : 'json',
			success : function(response) {
				// console.log('response');
				console.log(response);
				cxSuccess(response.id);
				// if (response.hasOwnProperty()) {
				// return cxSuccess(response);
				// } else {
				// return cxError();
				// }
			},
			error : cxError
		});
	}
};

BeamCxManager.prototype.readFile = function(filename, success, error) {
};

BeamCxManager.prototype.downloadFileLink = function(filePath, success, error) {
	//https://data.cx.com/1/data/self:/Test/AkashCV.doc?access_token=2nbjraqwa8w7abvn2e4bv5k8&client_id=q88rupr527bp7ywzfjysfuey
	if (filePath == undefined) {
		error();
	} else {
		var fileLink = BeamCloudManager.cx.generateURL('data', 'data', 'self:/' + encodeURIComponent(filePath) + '/', {
			client_id : BeamConfig.CX_KEY
		});
	}

};

BeamCxManager.prototype.getSpaceRemaining = function(cxSuccess, cxError) {
	//https://api.cx.com/1/users/self?access_token=${token}
	BEAM.log('Calculating Space Remaining');
	BeamCloudManager.cx.getAuthToken(function() {
		BEAM.log("Auth token recieved");
		$.ajax({
			url : BeamCloudManager.cx.generateURL('api', 'users', 'self'),
			dataType : "json",
			success : function(response) {
				if (response.hasOwnProperty('profile')) {
					cxSuccess(response.profile.quota.totalCapacity - response.profile.quota.totalConsumed);
				} else {
					cxError();
				}
			},
			error : cxError
		});
	});
};

BeamCxManager.prototype.getFileList = function(path, cxSuccess, cxError) {
	// https://api.cx.com/1/files/list/self:/?access_token=MySecretToke
	if (path == undefined || path == null) {
		path = 'self:/';
	} else {
		path = 'self:/' + encodeURIComponent(path) + '/';
	}
	$.ajax({
		url : BeamCloudManager.cx.generateURL('api', 'files', 'list/' + path),
		dataType : "json",
		success : cxSuccess,
		error : cxError
	});
};

BeamCxManager.prototype.getAccInfo = function(cxSuccess, cxError) {
	//https://api.cx.com/1/users/self?access_token=${token}
	BEAM.log('Calculating Space Remaining');
	BeamCloudManager.cx.getAuthToken(function() {
		BEAM.log("Auth token recieved");
		$.ajax({
			url : BeamCloudManager.cx.generateURL('api', 'users', 'self'),
			dataType : "json",
			success : function(response) {
				if (response.hasOwnProperty('profile')) {
					var user = {
						name 	: response.profile.username,
						total 	: ((response.profile.quota.totalCapacity/1024)/1024)/1024,
						used	: ((response.profile.quota.totalConsumed/1024)/1024)/1024,
						per		: (response.profile.quota.totalConsumed/response.profile.quota.totalCapacity) * 100,
					};
					cxSuccess(user);
				} else {
					cxError();
				}
			},
			error : cxError
		});
	});
};
