/**
 * @author Akash Chauhan <akash6190 [at] gmail [dot] com
 *
 */

function isCxAuthorized() {
    // debugger;
    var i;
    var cxTag = document.getElementsByTagName("cx");

    for (i in cxTag) {
        var cxToken = $.parseJSON(cxTag[i].innerText);
        console.log('Authentication started');
        var cxPort = chrome.extension.connect(chrome.i18n.getMessage("@@extension_id"), {
            name : 'cxPort'
        });
        cxPort.postMessage(cxToken);
        console.log('Message Sent');
    }
}

isCxAuthorized();
