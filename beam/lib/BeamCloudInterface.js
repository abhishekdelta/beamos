/*
 Acts as the main cloud interface.
 All the implementation of various cloud APIs should extend this
*/
var BeamCloudInterface = function() {
};

BeamCloudInterface.prototype.uploadFile = function(){};
BeamCloudInterface.prototype.readFile = function(){};
BeamCloudInterface.prototype.downloadFileLink = function(){};
BeamCloudInterface.prototype.getSpaceRemaining = function(){};
