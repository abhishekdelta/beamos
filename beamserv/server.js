var express = require('express')
  , everyauth = require('everyauth')
  , conf = require('./conf')
  , routes = require('./routes');

everyauth.debug = true;

var usersById = {};
var nextUserId = 0;

function addUser (source, sourceUser) {
  var user;
  if (arguments.length === 1) { // password-based
    user = sourceUser = source;
    user.id = ++nextUserId;
    return usersById[nextUserId] = user;
  } else { // non-password-based
    user = usersById[++nextUserId] = {id: nextUserId};
    user[source] = sourceUser;
  }
  return user;
}

var usersByGoogleId = {};

everyauth.everymodule.findUserById( function (id, callback) {
    callback(null, usersById[id]);
});
everyauth.everymodule.moduleTimeout(-1);

everyauth.google
  .appId('141809748517-m642qrbbe7arrld22b4hgl906vvpk7d0.apps.googleusercontent.com')
  .appSecret('9YujOPA6USjzzPNwe7yH_0Cv')
  .scope('https://www.googleapis.com/auth/userinfo.profile')
  .findOrCreateUser( function (sess, accessToken, extra, googleUser) {
    googleUser.refreshToken = extra.refresh_token;
    googleUser.expiresIn = extra.expires_in;
    return usersByGoogleId[googleUser.id] || (usersByGoogleId[googleUser.id] = addUser('google', googleUser));
  })
  .redirectPath('/beamos');

var app = express();
app.use(express.static(__dirname + '/public'))
  .use(express.favicon())
  .use(express.bodyParser())
  .use(express.cookieParser('htuayreve'))
  .use(express.session())
  .use(app.router)
  .use(everyauth.middleware(app));

app.configure( function () {
  app.set('view engine', 'ejs');
  app.set('views','./views');
});

app.get('/beamos/login/status', function (req, res) {
  if(everyauth) {
    res.send({status:0,loginStatus:req.loggedIn});
  } else {
    console.log("Everyauth not defined when checking login status.");
    res.send({status:1,msg:"everyauth not defined"});
  }
});

app.get('/', routes.index);

app.get('/beamos', function (req, res) {
  if(req.loggedIn) {
    res.render("home");
  } else {
    console.log("Logged out");
    console.log(req.loggedIn);
    res.send({status:0,msg:"logged out",loginStatus:req.loggedIn});
  }
});

app.get('/beamos/logout', function (req, res) {
  req.logout();
  res.send({status:0,msg:"logged out",loginStatus:req.loggedIn});
});

app.listen(3000);

module.exports = app;
